﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Input;

namespace Dark
{
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;

    public enum DoorType
    {
        Normal, Edge
    }

    public enum DoorPosition
    {
        Left, LeftTop, LeftBot, Right, RightTop, RightBot
    }

    public class Door : GameObject
    {
        private Matrix World = Matrix.Identity;
        private Matrix WorldInverseTranspose;

        private Vector3 hinge;
        // centre point of door
        private float orientation;

        private Vector3 room_centre;
        private DoorPosition position;
        private DoorType type;

        private float door_position_distance;

        float height;
        float width;
        float thickness;

        private Vector3 eye, up;

        float angle;
        float rad_angle;

        bool opening;
        bool closing;
        bool open;
        bool closed;

        int direction;

        float rotX = 0;
        float rotY = 0;

        float sin30 = 0.5f;
        float cos30 = (float)Math.Sqrt(3) / 2;

        float open_time = 0;

        //Effect effect;

        public Door(DarkGame game, Vector3 room_centre, 
            DoorType type, DoorPosition position, MazeProperties config)
        {
            this.room_centre = room_centre;
            this.type = type;
            this.position = position;

            height = config.door_height;
            width = config.door_width;
            thickness = config.room_thickness * 2f;

            door_position_distance = (config.inner_radius + config.room_thickness) * cos30;

            collisionRadius = 0.125f;
            calculatePosition();


            hinge = new Vector3(-width/2, 0.0f, 0.0f);

            Vector3 frontNormal = new Vector3(0.0f, 0.0f, -1.0f);
            Vector3 backNormal = new Vector3(0.0f, 0.0f, 1.0f);
            Vector3 topNormal = new Vector3(0.0f, 1.0f, 0.0f);
            Vector3 bottomNormal = new Vector3(0.0f, -1.0f, 0.0f);
            Vector3 leftNormal = new Vector3(-1.0f, 0.0f, 0.0f);
            Vector3 rightNormal = new Vector3(1.0f, 0.0f, 0.0f);



            vertices = Buffer.Vertex.New(
                game.GraphicsDevice,
                new[]
                    {
                        new VertexPositionNormalColor(new Vector3(-width / 2f, height, -thickness / 2f), topNormal, Color.OrangeRed), // Top
                        new VertexPositionNormalColor(new Vector3(-width / 2f, height, thickness / 2f), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(width / 2f, height, thickness / 2f), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-width / 2f, height, -thickness / 2f), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(width / 2f, height, thickness / 2f), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(width / 2f, height, -thickness / 2f), topNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(-width / 2f, 0.0f, -thickness / 2f), bottomNormal, Color.OrangeRed), // Bottom
                        new VertexPositionNormalColor(new Vector3(width / 2f, 0.0f, thickness / 2f), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-width / 2f, 0.0f, thickness / 2f), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-width / 2f, 0.0f, -thickness / 2f), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(width / 2f, 0.0f, -thickness / 2f), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(width / 2f, 0.0f, thickness / 2f), bottomNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(-width / 2f, 0.0f, -thickness / 2f), leftNormal, Color.DarkOrange), // Left
                        new VertexPositionNormalColor(new Vector3(-width / 2f, 0.0f, thickness / 2f), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-width / 2f, height, thickness / 2f), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-width / 2f, 0.0f, -thickness / 2f), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-width / 2f, height, thickness / 2f), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-width / 2f, height, -thickness / 2f), leftNormal, Color.DarkOrange),

                        new VertexPositionNormalColor(new Vector3(width / 2f, 0.0f, -thickness / 2f), rightNormal, Color.DarkOrange), // Right
                        new VertexPositionNormalColor(new Vector3(width / 2f, height, thickness / 2f), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(width / 2f, 0.0f, thickness / 2f), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(width / 2f, 0.0f, -thickness / 2f), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(width / 2f, height, -thickness / 2f), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(width / 2f, height, thickness / 2f), rightNormal, Color.DarkOrange),

                        new VertexPositionNormalColor(new Vector3(-width / 2f, 0.0f, -thickness / 2f), frontNormal, Color.Gray), // Front
                        new VertexPositionNormalColor(new Vector3(-width / 2f, height, -thickness / 2f), frontNormal, Color.Gray),
                        new VertexPositionNormalColor(new Vector3(width / 2f, height, -thickness / 2f), frontNormal, Color.Gray),
                        new VertexPositionNormalColor(new Vector3(width / 2f, height, -thickness / 2f), frontNormal, Color.Gray),
                        new VertexPositionNormalColor(new Vector3(width / 2f, 0.0f, -thickness / 2f), frontNormal, Color.Gray),
                        new VertexPositionNormalColor(new Vector3(-width / 2f, 0.0f, -thickness / 2f), frontNormal, Color.Gray),

                        new VertexPositionNormalColor(new Vector3(-width / 2f, 0.0f, thickness / 2f), backNormal, Color.Gray), // Back
                        new VertexPositionNormalColor(new Vector3(width / 2f, 0.0f, thickness / 2f), backNormal, Color.Gray),
                        new VertexPositionNormalColor(new Vector3(width / 2f, height, thickness / 2f), backNormal, Color.Gray),
                        new VertexPositionNormalColor(new Vector3(width / 2f, height, thickness / 2f), backNormal, Color.Gray),
                        new VertexPositionNormalColor(new Vector3(-width / 2f, height, thickness / 2f), backNormal, Color.Gray),
                        new VertexPositionNormalColor(new Vector3(-width / 2f, 0.0f, thickness / 2f), backNormal, Color.Gray),

                    });

            opening = false;
            closing = false;
            open = false;
            closed = true;

            direction = 1;

            rad_angle = 0;
            angle = 0;

            eye = new Vector3(0, 0, -10);
            up = new Vector3(Vector3.UnitY.X, Vector3.UnitY.Y, Vector3.UnitY.Z);


            effect = game.Content.Load<Effect>("Gouraud");
            
            //effect.Parameters["View"].SetValue(game.camera.View);
            //effect.Parameters["Projection"].SetValue( game.camera.Projection);
            //effect.Parameters["World"].SetValue(Matrix.Identity);
            //effect.Parameters["VertexColorEnabled"].SetValue(true);
            //effect.Parameters["LightingEnabled"].SetValue(true);

            //effect.DirectionalLight0.Direction = new Vector3(-1, -1, 0);
            //effect.DirectionalLight0.SpecularColor = new Vector3(1, 1, 1);
            //effect.DirectionalLight0.DiffuseColor = new Vector3(1, 1, 1);


            inputLayout = VertexInputLayout.FromBuffer(0, vertices);
            this.game = game;
        }

        public override void Update(GameTime gameTime)
        {
            float time = (float)gameTime.TotalGameTime.TotalSeconds;
            float timechange = (float)gameTime.ElapsedGameTime.TotalSeconds * 2;

            if (game.keyboardState.IsKeyDown(Keys.Space) || (open && time - open_time > 1))
            {
                if (open)
                {
                    closing = true;
                    open = false;
                    closed = false;
                }
                else if (closed)
                {
                    opening = true;
                    open = false;
                    closed = false;
                    
                }                
            }

            if (closed && game.keyboardState.IsKeyDown(Keys.C))
            {
                if (direction == 1)
                {
                    direction = -1;
                }
                else
                {
                    direction = 1;
                }
            }

            if (opening)
            {
                Open(timechange*2);
            }
            else if (closing)
            {
                Close(timechange*2);
            }

            angle = (float)(rad_angle * 180/Math.PI);
            angle = angle - (int)(angle / 360) * 360;

            if (rad_angle > Math.PI / 2)
            {
                angle = 90;
                rad_angle = (float)Math.PI / 2;
                opening = false;
                open = true;
                closed = false;
                if (open_time == 0)
                {
                    open_time = time;
                }
                
            }

            if (angle < 0)
            {
                angle = 0;
                rad_angle = 0;
                closing = false;
                closed = true;
                open = false;
                open_time = 0;
            }

            World = Matrix.Translation(hinge) * Matrix.RotationY(direction * rad_angle) * Matrix.Translation(-hinge) *
                Matrix.RotationY(orientation) * Matrix.Translation(pos) * Matrix.RotationY(rotY) * Matrix.RotationX(rotX);
            WorldInverseTranspose = Matrix.Transpose(Matrix.Invert(World));

        }

        public override void Draw(GameTime gameTime)
        {
            // Setup the effect parameters
            effect.Parameters["World"].SetValue(World);
            effect.Parameters["Projection"].SetValue(game.camera.Projection);
            effect.Parameters["View"].SetValue(game.camera.View);
            effect.Parameters["cameraPos"].SetValue(game.camera.pos);
            effect.Parameters["worldInvTrp"].SetValue(WorldInverseTranspose);
            effect.Parameters["lightPntPos"].SetValue(game.world.lantern.pos);
            effect.Parameters["intensity"].SetValue(game.world.player.energyIntensity());

            // Setup the vertices
            game.GraphicsDevice.SetVertexBuffer(vertices);
            game.GraphicsDevice.SetVertexInputLayout(inputLayout);

            // Apply the basic effect technique and draw the rotating cube
            effect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, vertices.ElementCount);
        }

        public void Open(float time)
        {
            rad_angle += time;
        }

        public void Close(float time)
        {
            rad_angle -= time;
        }

        public void calculatePosition()
        {
            if (position == DoorPosition.Left)
            {
                pos = new Vector3(room_centre.X - door_position_distance, 0f, room_centre.Z);
                orientation = -(float)Math.PI / 2;
            }
            else if (position == DoorPosition.Right)
            {
                pos = new Vector3(room_centre.X + door_position_distance, 0f, room_centre.Z);
                orientation = (float)Math.PI / 2;
            }
            else if (position == DoorPosition.LeftTop)
            {
                pos = new Vector3(room_centre.X - door_position_distance * sin30, 0f, room_centre.Z + door_position_distance * cos30);
                orientation = -(float)Math.PI / 6;
            }
            else if (position == DoorPosition.RightTop)
            {
                pos = new Vector3(room_centre.X + door_position_distance * sin30, 0f, room_centre.Z + door_position_distance * cos30);
                orientation = (float)Math.PI / 6;
            }
            else if (position == DoorPosition.LeftBot)
            {
                pos = new Vector3(room_centre.X - door_position_distance * sin30, 0f, room_centre.Z - door_position_distance * cos30);
                orientation = (float)Math.PI / 6;
            }
            else if (position == DoorPosition.RightBot)
            {
                pos = new Vector3(room_centre.X + door_position_distance * sin30, 0f, room_centre.Z - door_position_distance * cos30);
                orientation = -(float)Math.PI / 6;
            }
        }

        public void setOpeningFlag(bool value)
        {
            this.opening = value;
        }

        public void setClosingFlag(bool value)
        {
            this.closing = value;
        }

        public void setOpenFlag(bool value)
        {
            this.open = value;
        }

        public void setCloseFlag(bool value)
        {
            this.closed = value;
        }
        public float getDoorRadius()
        {
            return this.door_position_distance;
        }

        public DoorType getDoorType()
        {
            return this.type;
        }

        public DoorPosition getDoorPosition()
        {
            return this.position;
        }
    }
}
