﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using SharpDX;
using SharpDX.Toolkit;
using System;
using System.Collections.Generic;
using Windows.UI.Input;
using Windows.UI.Core;
using Windows.Devices.Sensors;

namespace Dark
{
    // Use this namespace here in case we need to use Direct3D11 namespace as well, as this
    // namespace will override the Direct3D11.
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;
    /// <summary>
    ///     The Game class is responsible for the flow of the game as well as receiving input
    /// from the user. It is also responsible for drawing into the screen. It contains a World
    /// object which contains all gameObjects in the game and a Controller object which will
    /// handle updates to the world.
    /// </summary>
    public class DarkGame : Game
    {
        public World world;
        public Logic logic;

        private GraphicsDeviceManager graphicsDeviceManager;

        // Keyboard Inputs
        private KeyboardManager keyboardManager;
        public KeyboardState keyboardState;

        // Accelerometer
        public Accelerometer accelerometer;
        public AccelerometerReading accelerometerReading;

        public MainPage mainPage;

        // Represents the camera's position and orientation
        public Camera camera;

        // Random number generator
        public Random random;

        public bool started = false;
        public bool isHintView = false;
        public bool prevIsHintView = false;
        public float hintViewTime;
        public bool isPaused = false;
        public bool isGameOver = false;
        /// <summary>
        /// Initializes a new instance of the <see cref="DarkGame" /> class.
        /// </summary>
        public DarkGame(MainPage mainPage)
        {
            world = new World(this);
            logic = new Logic(world);

            // Creates a graphics manager. This is mandatory.
            graphicsDeviceManager = new GraphicsDeviceManager(this);

            // Setup the relative directory to the executable directory
            // for loading contents with the ContentManager
            Content.RootDirectory = "Content";

            // Create the keyboard manager
            keyboardManager = new KeyboardManager(this);
            accelerometer = Accelerometer.GetDefault();

            random = new Random();

            this.mainPage = mainPage;

        }

        protected override void LoadContent()
        {
            world.LoadContent();
            //shapes.Add(new Cube(this));
            camera.Initialize();
            base.LoadContent();
        }

        protected override void Initialize()
        {
            Window.Title = "Dark";
            camera = new Camera(this);
            base.Initialize();
        }

        private bool checkPlayerGetKilled()
        {
            Player current_player = this.world.player;
            foreach (var obj in this.world.gameObjects)
            {
                if (obj is Enemy)
                {
                    Enemy e = (Enemy)obj;
                    if (Vector3.Distance(e.pos, current_player.pos) < (current_player.collisionRadius + e.collisionRadius))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        protected override void Update(GameTime gameTime)
        {
            float time = (float)gameTime.TotalGameTime.TotalSeconds;
            float timechange = (float)gameTime.ElapsedGameTime.TotalSeconds * 2;

            if (started && !isPaused && !isGameOver)
            {
                // If game has started and is not paused

                if (isHintView && !prevIsHintView)
                {
                    // If hintView just started, set hintView as true and start timer
                    hintViewTime = time;
                    prevIsHintView = isHintView;
                    camera.isHintView = true;
                }
                else if (isHintView && prevIsHintView && ((time - hintViewTime) >= 3))
                {
                    // If hintView is still going on, if 3 sec has passed since the hintView started, set hintView as false
                    isHintView = false;
                    prevIsHintView = false;
                    camera.isHintView = false;
                }
                if (!isHintView)
                {
                    camera.isHintView = true;
                    // Get Keyboard State
                    keyboardState = keyboardManager.GetState();

                    // rotation doesn't affected by collision
                    // Set Rotation movement
                    Player currPlayer = this.world.player;
                    if (keyboardState.IsKeyDown(Keys.Left)) { currPlayer.rotateLeft(); }
                    if (keyboardState.IsKeyDown(Keys.Right)) { currPlayer.rotateRight(); }

                    accelerometerReading = accelerometer.GetCurrentReading();

                    // check collision before moving player front/back
                    // Set Translation movement
                    if (keyboardState.IsKeyDown(Keys.Up)) { currPlayer.moveFront(currPlayer.checkWallCollision()); }
                    if (keyboardState.IsKeyDown(Keys.Down)) { currPlayer.moveBack(); }

                    if (keyboardState.IsKeyDown(Keys.Escape))
                    {
                        this.Exit();
                        this.Dispose();
                        App.Current.Exit();
                    }
                }

                camera.Update(gameTime);
                logic.update(gameTime);
                world.update(gameTime);
                //shapes[0].Update(gameTime);

                if (isEndGame())
                {
                    isGameOver = true;
                    mainPage.GameOver();
                }

                // Handle base.Update
                base.Update(gameTime);
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            if (started && !isGameOver)
            {
                GraphicsDevice.Clear(Color.Black);

                world.draw(gameTime);
            }
            base.Draw(gameTime);
        }

        public void HintView()
        {
            if (this.world.player.energy > this.world.player.maxEnergy * 0.2f)
            {
                if (!this.isHintView)
                {
                    this.world.player.energy -= this.world.player.maxEnergy * 0.2f;
                }
                this.isHintView = true;
                
            }
            
        }

        public bool isEndGame()
        {
            if (world.player.energy == 0)
            {
                return true;
            }
            else if (world.player.isDead)
            {
                return true;
            }
            else if (!world.maze.isInMaze(world.player.pos))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
