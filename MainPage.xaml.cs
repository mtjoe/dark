﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using SharpDX;

namespace Dark
{
    using Windows.UI;
    using Windows.UI.Xaml.Media;
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        public MainMenu mainMenu;
        public GameOver gameOver;

        public DarkGame game;

        public MainPage()
        {
            InitializeComponent();
            game = new DarkGame(this);
            game.Run(this);
            mainMenu = new MainMenu(this);
            gameOver = new GameOver(this);
            this.DropVisibilityAll();
            this.Children.Add(mainMenu);
        }

        public void Restart()
        {
            InitializeComponent();
            game = new DarkGame(this);
            game.Run(this);
            mainMenu = new MainMenu(this);
            gameOver = new GameOver(this);
            this.DropVisibilityAll();
            this.Children.Add(mainMenu);
        }

        private void DropVisibilityAll()
        {
            this.DistIndicator.Visibility = Visibility.Collapsed;
            this.EnergyIndicator.Visibility = Visibility.Collapsed;
            this.btn_hint.Visibility = Visibility.Collapsed;
            this.btn_pause.Visibility = Visibility.Collapsed;
            this.btn_run.Visibility = Visibility.Collapsed;

            // Pause Menu
            this.pause_rec.Visibility = Visibility.Collapsed;
            this.btn_resume.Visibility = Visibility.Collapsed;
            this.btn_main_menu.Visibility = Visibility.Collapsed;
            this.btn_exit.Visibility = Visibility.Collapsed;
        }


        private void enableVisibilityAll()
        {
            this.DistIndicator.Visibility = Visibility.Visible;
            this.EnergyIndicator.Visibility = Visibility.Visible;
            this.btn_hint.Visibility = Visibility.Visible;
            this.btn_pause.Visibility = Visibility.Visible;
            this.btn_run.Visibility = Visibility.Visible;
        }

        public void StartGame()
        {
            this.Children.Remove(mainMenu);
            this.enableVisibilityAll();
            game.started = true;
        }

        private void Hint(object sender, RoutedEventArgs e)
        {
            game.HintView();
        }

        /**
         * Takes infurthe value from 0 to 100, 100 being closest to goal, and 0 being furthest away from goal, updates 
         * distance indicator's color, such 
         */
        public void UpdateDistanceIndicator(int value)
        {
            SolidColorBrush brush = new SolidColorBrush();
            brush.Color = Color.FromArgb(255, (byte)(value * 2.55f), (byte)(255 - (value * 2.55f)), 50);
            this.DistIndicator.Fill = brush;
        }

        public void UpdateEnergyIndicator(int value)
        {
            SolidColorBrush brush = new SolidColorBrush();
            brush.Color = Color.FromArgb(255, (byte)(255 - (value * 2.55f)), (byte)(value * 2.55f), 50);
            this.EnergyIndicator.Foreground = brush;
            this.EnergyIndicator.Text = value + "%";
        }

        private void Pause(object sender, RoutedEventArgs e)
        {
            game.isPaused = true;
            // Pause Menu
            this.pause_rec.Visibility = Visibility.Visible;
            this.btn_resume.Visibility = Visibility.Visible;
            this.btn_main_menu.Visibility = Visibility.Visible;
            this.btn_exit.Visibility = Visibility.Visible;
        }

        private void Resume(object sender, RoutedEventArgs e)
        {
            game.isPaused = false;
            // Pause Menu
            this.pause_rec.Visibility = Visibility.Collapsed;
            this.btn_resume.Visibility = Visibility.Collapsed;
            this.btn_main_menu.Visibility = Visibility.Collapsed;
            this.btn_exit.Visibility = Visibility.Collapsed;
        }

        private void MainMenu(object sender, RoutedEventArgs e)
        {
            this.game.Exit();
            this.game.Dispose();
            game = new DarkGame(this);
            this.Children.Add(mainMenu);
            this.Children.Remove(this);
            DropVisibilityAll();
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            App.Current.Exit();
        }
        public void GameOver()
        {
            this.Children.Add(gameOver);
            this.Children.Remove(this);
            DropVisibilityAll();
        }

        public void updateScore()
        {
            this.txt_score.Text = this.game.world.player.GetScore().ToString();
            gameOver.updateScore();
        }

        public bool isRun()
        {
            return this.btn_run.IsPressed;
        }


    }
}