﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;
using Windows.UI.Input;
using Windows.UI.Core;

namespace Dark
{
    using SharpDX.Toolkit.Graphics;

    abstract public class GameObject
    {
        public BasicEffect basicEffect;
        public Effect effect;
        public VertexInputLayout inputLayout;
        public Buffer<VertexPositionNormalColor> vertices;
        public Buffer<VertexPositionNormalTexture> texture_vertices;
        public DarkGame game;
        public Vector3 pos;
        public float collisionRadius;

        public abstract void Update(GameTime gametime);
        public abstract void Draw(GameTime gametime);
        // These virtual voids allow any object that extends GameObject to respond to tapped and manipulation events
        public virtual void Tapped(GestureRecognizer sender, TappedEventArgs args)
        {

        }

        public virtual void OnManipulationStarted(GestureRecognizer sender, ManipulationStartedEventArgs args)
        {

        }

        public virtual void OnManipulationUpdated(GestureRecognizer sender, ManipulationUpdatedEventArgs args)
        {

        }

        public virtual void OnManipulationCompleted(GestureRecognizer sender, ManipulationCompletedEventArgs args)
        {

        }
    }
}
