﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using SharpDX;
using SharpDX.Toolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dark
{
    public class World
    {
        // ----- Instance Variables -----

        public DarkGame game;

        public Maze maze;
        public MazeProperties config;
        public List<GameObject> gameObjects;
        public Player player;
        public Cube goal;
        public Lantern lantern;
        public int number_enemies;

        public Dictionary<Room, List<Room>> roomToNeighbourRoomDict;
        public World(DarkGame game)
        {
            this.game = game;
        }

        public void LoadContent()
        {
            number_enemies = 1;
            // Initialise game object containers.
            gameObjects = new List<GameObject>();

            config = new MazeProperties()
            {
                rows = 9,
                roomsPerRow = 4,
                inner_radius = 6.0f,
                room_thickness = 0.125f,
                room_height = 4.5f,
                platform_level = -2.0f,
                door_height = 4.0f,
                door_width = 3.0f
            };

            maze = new Maze(this.game,config);
            roomToNeighbourRoomDict = new Dictionary<Room, List<Room>>();

            // creating a dictionary that store room to neighbourRoom 
            foreach (Room room in maze.rooms)
            {
                List<Room> tempList = new List<Room>();
                foreach (Room neighbour in maze.rooms)
                {
                    if (!room.Equals(neighbour))
                    {
                        if (Vector3.Distance(room.pos, neighbour.pos) < 14.0f)
                        {
                            tempList.Add(neighbour);
                        }
                    }
                }
                roomToNeighbourRoomDict.Add(room, tempList);
            }

            // Create game objects.
            player = new Player(this.game);

            for (int i = 0; i < number_enemies; i++)
            {
                gameObjects.Add(new Enemy(this.game, maze.getEnemyRoom().getPos()));
            }

            goal = new Cube(this.game);
            lantern = new Lantern(this.game);
        }

        public void update(GameTime gameTime)
        {
            // Check distance of player to goal and set the distance indicator
            float distToGoal = Vector3.Distance(goal.getPos(), player.getPos());
            game.mainPage.UpdateDistanceIndicator((int)((distToGoal/maze.MAXDISTANCE) * 100));

            game.mainPage.UpdateEnergyIndicator((int)((player.energy/player.maxEnergy) * 100));
            
            // Check whether player collide with goal
            if (distToGoal <= goal.collisionR)
            {
                goal.setRandomPos();
                player.AddScore(10);
                player.increaseEnergy();
                this.game.mainPage.updateScore();
            }

            // Update all GameObjects in World
            for (int i = 0; i < gameObjects.Count; i++)
            {
                gameObjects[i].Update(gameTime);
            }
            player.Update(gameTime);
            maze.Update(gameTime);
            goal.Update(gameTime);
            lantern.Update(gameTime);
        }

        public void draw(GameTime gameTime)
        {
            for (int i = 0; i < gameObjects.Count; i++)
            {
                gameObjects[i].Draw(gameTime);
            }
            player.Draw(gameTime);
            maze.Draw(gameTime);
            goal.Draw(gameTime);
            lantern.Draw(gameTime);
        }

        public bool checkObjectInDoor(Vector3 pos)
        {
            foreach (Door door in this.maze.doors)
            {
                if (Vector3.Distance(door.pos, pos) < (door.getDoorRadius()-this.player.collisionRadius))
                {
                    return true;
                }
            }
            return false;
        }

        public Door getDoorByPlayerPosition(Vector3 pos)
        {
            Player current_player = this.player;
            foreach (Door door in this.maze.doors)
            {
                if (Vector3.Distance(door.pos, pos) < (door.getDoorRadius()-this.player.collisionRadius))
                {
                    return door;
                }
            }
            // shouldn't happen
            return null;
        }

        public bool checkObjectInRoom(Vector3 pos)
        {
            foreach (Room room in this.maze.rooms)
            {
                if (Vector3.Distance(room.pos, pos) < room.getInRadius())
                {
                    return true;
                }
            }
            return false;
        }
        /**
         * return the center of the room position given object position
         **/
        public Room getRoomByPlayerPosition(Vector3 pos)
        {
            foreach (Room room in this.maze.rooms)
            {
                if (Vector3.Distance(room.pos, pos) < room.getInRadius())
                {
                    return room;
                }
            }
            return null;
        }

        public List<Door> getDoorListByRoom(Room room)
        {
            List<Door> doorList = new List<Door>();
            float roomOutRadius = room.getOutRadius();
            Vector3 roomPos = room.pos;
            foreach (Door door in this.game.world.maze.doors)
            {
                if (Vector3.Distance(door.pos, roomPos) < roomOutRadius)
                {
                    doorList.Add(door);
                }
            }
            return doorList;
        }

        public Vector3 GetRandomRoom()
        {
            Random rnd = new Random();
            return this.maze.rooms[rnd.Next(0, this.maze.rooms.Count)].pos;
        }

    }
}
