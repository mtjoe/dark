﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;

namespace Dark
{
    using SharpDX.Toolkit.Graphics;

    abstract public class Shape
    {
        public Effect effect;
        public VertexInputLayout inputLayout;
		public Buffer<VertexPositionNormalColor> vertices;
        public DarkGame game;

        public abstract void Update(GameTime gametime);
        public abstract void Draw(GameTime gametime);

        public abstract SharpDX.Vector3 getPos();
    }
}
