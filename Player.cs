﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;
using Windows.UI.Input;
using Windows.UI.Core;

namespace Dark
{
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;
    // Player class.
    public class Player : GameObject
    {
        private Matrix World = Matrix.Identity;
        private Matrix WorldInverseTranspose;

        public bool isDead = false;

        public float velocity;
        private Vector3 target;
        private Vector3 initial_target;
        private float rotation;

        private int score;
        public float energy;
        public float maxEnergy;

        private float height = 4.0f;

        // For collision checking.. assume user is in safe state in the beginning
        private bool isInBigCircle = true;
        // head
        private float head_height_max = 4.0f;
        private float head_height_min = 3.1f;
        private float head_size;
        // body
        private float body_height_max = 3.0f;
        private float body_height_min = 1.5f;
        private float body_width = 0.4f;
        private float body_thickness = 0.25f;
        // body part
        private float body_part_width = 0.25f;
        // arms
        private float arms_x1 = 0.5f;
        private float arms_x2 = 0.5f + 0.3f;
        // legs
        private float legs_height_max = 1.5f;
        private float legs_height_min = 0.0f;
        private float legs_x1 = 0.1f;
        private float legs_x2 = 0.1f + 0.4f;

        public Player(DarkGame game)
        {
            pos = game.world.maze.getRandomRoom().getPos();


            initial_target = new Vector3(0, 0, -1);
            target = initial_target;
            rotation = 0.0f;
            velocity = 0.05f;
            head_size = (head_height_max - head_height_min) / 2;

            Vector3 normal = new Vector3(0, 0, 0);

            maxEnergy = 1000.0f;
            energy = maxEnergy;

            collisionRadius = (body_part_width + arms_x1 + arms_x2);

            Vector3 frontNormal = new Vector3(0.0f, 0.0f, -1.0f);
            Vector3 backNormal = new Vector3(0.0f, 0.0f, 1.0f);
            Vector3 topNormal = new Vector3(0.0f, 1.0f, 0.0f);
            Vector3 bottomNormal = new Vector3(0.0f, -1.0f, 0.0f);
            Vector3 leftNormal = new Vector3(-1.0f, 0.0f, 0.0f);
            Vector3 rightNormal = new Vector3(1.0f, 0.0f, 0.0f);

            vertices = Buffer.Vertex.New(
                game.GraphicsDevice,
                new[]
                    {
                        // Head
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_min, -head_size), frontNormal, Color.Orange), // Front
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_max, -head_size), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_max, -head_size), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_min, -head_size), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_max, -head_size), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_min, -head_size), frontNormal, Color.Orange),

                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_min, head_size), backNormal, Color.Orange), // BACK
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_max, head_size), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_max, head_size), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_min, head_size), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_min, head_size), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_max, head_size), backNormal, Color.Orange),

                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_max, -head_size), topNormal, Color.OrangeRed), // Top
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_max, head_size), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_max, head_size), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_max, -head_size), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_max, head_size), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_max, -head_size), topNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_min, -head_size), bottomNormal, Color.OrangeRed), // Bottom
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_min, head_size), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_min, head_size), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_min, -head_size), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_min, -head_size), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_min, head_size), bottomNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_min, -head_size), leftNormal, Color.DarkOrange), // Left
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_min, head_size), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_max, head_size), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_min, -head_size), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_max, head_size), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-head_size, head_height_max, -head_size), leftNormal, Color.DarkOrange),

                        new VertexPositionNormalColor(new Vector3(head_size, head_height_min, -head_size), rightNormal, Color.DarkOrange), // Right
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_max, head_size), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_min, head_size), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_min, -head_size), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_max, -head_size), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(head_size, head_height_max, head_size), rightNormal, Color.DarkOrange),

                        // body
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_min, -body_thickness), frontNormal, Color.Orange), // Front
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_max, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_max, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_min, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_max, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_min, -body_thickness), frontNormal, Color.Orange),

                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_min, body_thickness), backNormal, Color.Orange), // BACK
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_max, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_max, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_min, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_min, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_max, body_thickness), backNormal, Color.Orange),

                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_max, -body_thickness), topNormal, Color.OrangeRed), // Top
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_max, -body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_max, -body_thickness), topNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_min, -body_thickness), bottomNormal, Color.OrangeRed), // Bottom
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_min, body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_min, body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_min, -body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_min, -body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_min, body_thickness), bottomNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_min, -body_thickness), leftNormal, Color.DarkOrange), // Left
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_min, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_max, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_min, -body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_max, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-body_width, body_height_max, -body_thickness), leftNormal, Color.DarkOrange),

                        new VertexPositionNormalColor(new Vector3(body_width, body_height_min, -body_thickness), rightNormal, Color.DarkOrange), // Right
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_max, body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_min, body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_min, -body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_max, -body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(body_width, body_height_max, body_thickness), rightNormal, Color.DarkOrange),

                        // left arm
                        new VertexPositionNormalColor(new Vector3(arms_x1, body_height_min, -body_thickness), frontNormal, Color.Orange), // Front
                        new VertexPositionNormalColor(new Vector3(arms_x1 - 0.1f, body_height_max, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(arms_x2 - 0.1f, body_height_max, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(arms_x1, body_height_min, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(arms_x2 - 0.1f, body_height_max, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(arms_x2, body_height_min, -body_thickness), frontNormal, Color.Orange),

                        new VertexPositionNormalColor(new Vector3(arms_x1, body_height_min, body_thickness), backNormal, Color.Orange), // BACK
                        new VertexPositionNormalColor(new Vector3(arms_x2 - 0.1f, body_height_max, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(arms_x1 - 0.1f, body_height_max, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(arms_x1, body_height_min, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(arms_x2, body_height_min, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(arms_x2 - 0.1f, body_height_max, body_thickness), backNormal, Color.Orange),

                        new VertexPositionNormalColor(new Vector3(arms_x1 - 0.1f, body_height_max, -body_thickness), topNormal, Color.OrangeRed), // Top
                        new VertexPositionNormalColor(new Vector3(arms_x1 - 0.1f, body_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(arms_x2 - 0.1f, body_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(arms_x1 - 0.1f, body_height_max, -body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(arms_x2 - 0.1f, body_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(arms_x2 - 0.1f, body_height_max, -body_thickness), topNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(arms_x1, body_height_min, -body_thickness), bottomNormal, Color.OrangeRed), // Bottom
                        new VertexPositionNormalColor(new Vector3(arms_x2, body_height_min, body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(arms_x1, body_height_min, body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(arms_x1, body_height_min, -body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(arms_x2, body_height_min, -body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(arms_x2, body_height_min, body_thickness), bottomNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(arms_x1, body_height_min, -body_thickness), leftNormal, Color.DarkOrange), // Left
                        new VertexPositionNormalColor(new Vector3(arms_x1, body_height_min, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(arms_x1 - 0.1f, body_height_max, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(arms_x1, body_height_min, -body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(arms_x1 - 0.1f, body_height_max, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(arms_x1 - 0.1f, body_height_max, -body_thickness), leftNormal, Color.DarkOrange),

                        new VertexPositionNormalColor(new Vector3(arms_x2, body_height_min, -body_thickness), rightNormal, Color.DarkOrange), // Right
                        new VertexPositionNormalColor(new Vector3(arms_x2 - 0.1f, body_height_max, body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(arms_x2, body_height_min, body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(arms_x2, body_height_min, -body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(arms_x2 - 0.1f, body_height_max, -body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(arms_x2 - 0.1f, body_height_max, body_thickness), rightNormal, Color.DarkOrange),

                        // right arm
                        new VertexPositionNormalColor(new Vector3(-arms_x2, body_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)), frontNormal, Color.Orange), // Front
                        new VertexPositionNormalColor(new Vector3(-arms_x2, body_height_max, -(body_height_max - body_height_min - body_thickness)), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-arms_x1, body_height_max, -(body_height_max - body_height_min - body_thickness)), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-arms_x2, body_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-arms_x1, body_height_max, -(body_height_max - body_height_min - body_thickness)), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-arms_x1, body_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)), frontNormal, Color.Orange),

                        new VertexPositionNormalColor(new Vector3(-arms_x2 + 0.1f, body_height_max - 0.4f, body_thickness), backNormal, Color.Orange), // BACK
                        new VertexPositionNormalColor(new Vector3(-arms_x1 + 0.1f, body_height_max, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-arms_x2 + 0.1f, body_height_max, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-arms_x2 + 0.1f, body_height_max - 0.4f, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-arms_x1 + 0.1f, body_height_max - 0.4f, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-arms_x1 + 0.1f, body_height_max, body_thickness), backNormal, Color.Orange),

                        new VertexPositionNormalColor(new Vector3(-arms_x2, body_height_max, -(body_height_max - body_height_min - body_thickness)), topNormal, Color.OrangeRed), // Top
                        new VertexPositionNormalColor(new Vector3(-arms_x2 + 0.1f, body_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-arms_x1 + 0.1f, body_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-arms_x2, body_height_max, -(body_height_max - body_height_min - body_thickness)), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-arms_x1 + 0.1f, body_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-arms_x1, body_height_max, -(body_height_max - body_height_min - body_thickness)), topNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(-arms_x2, body_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)), bottomNormal, Color.OrangeRed), // Bottom
                        new VertexPositionNormalColor(new Vector3(-arms_x1 + 0.1f, body_height_max - 0.4f, body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-arms_x2 + 0.1f, body_height_max - 0.4f, body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-arms_x2, body_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-arms_x1, body_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-arms_x1 + 0.1f, body_height_max - 0.4f, body_thickness), bottomNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(-arms_x2, body_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)), leftNormal, Color.DarkOrange), // Left
                        new VertexPositionNormalColor(new Vector3(-arms_x2 + 0.1f, body_height_max - 0.4f, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-arms_x2 + 0.1f, body_height_max, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-arms_x2, body_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-arms_x2 + 0.1f, body_height_max, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-arms_x2, body_height_max, -(body_height_max - body_height_min - body_thickness)), leftNormal, Color.DarkOrange),

                        new VertexPositionNormalColor(new Vector3(-arms_x1, body_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)), rightNormal, Color.DarkOrange), // Right
                        new VertexPositionNormalColor(new Vector3(-arms_x1 + 0.1f, body_height_max, body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-arms_x1 + 0.1f, body_height_max - 0.4f, body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-arms_x1, body_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-arms_x1, body_height_max, -(body_height_max - body_height_min - body_thickness)), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-arms_x1 + 0.1f, body_height_max, body_thickness), rightNormal, Color.DarkOrange),

                        // left leg
                        new VertexPositionNormalColor(new Vector3(legs_x1, legs_height_min, -body_thickness), frontNormal, Color.Orange), // Front
                        new VertexPositionNormalColor(new Vector3(legs_x1 - 0.1f, legs_height_max, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(legs_x2 - 0.1f, legs_height_max, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(legs_x1, legs_height_min, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(legs_x2 - 0.1f, legs_height_max, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(legs_x2, legs_height_min, -body_thickness), frontNormal, Color.Orange),

                        new VertexPositionNormalColor(new Vector3(legs_x1, legs_height_min, body_thickness), backNormal, Color.Orange), // BACK
                        new VertexPositionNormalColor(new Vector3(legs_x2 - 0.1f, legs_height_max, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(legs_x1 - 0.1f, legs_height_max, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(legs_x1, legs_height_min, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(legs_x2, legs_height_min, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(legs_x2 - 0.1f, legs_height_max, body_thickness), backNormal, Color.Orange),

                        new VertexPositionNormalColor(new Vector3(legs_x1 - 0.1f, legs_height_max, -body_thickness), topNormal, Color.OrangeRed), // Top
                        new VertexPositionNormalColor(new Vector3(legs_x1 - 0.1f, legs_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(legs_x2 - 0.1f, legs_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(legs_x1 - 0.1f, legs_height_max, -body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(legs_x2 - 0.1f, legs_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(legs_x2 - 0.1f, legs_height_max, -body_thickness), topNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(legs_x1, legs_height_min, -body_thickness), bottomNormal, Color.OrangeRed), // Bottom
                        new VertexPositionNormalColor(new Vector3(legs_x2, legs_height_min, body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(legs_x1, legs_height_min, body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(legs_x1, legs_height_min, -body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(legs_x2, legs_height_min, -body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(legs_x2, legs_height_min, body_thickness), bottomNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(legs_x1, legs_height_min, -body_thickness), leftNormal, Color.DarkOrange), // Left
                        new VertexPositionNormalColor(new Vector3(legs_x1, legs_height_min, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(legs_x1 - 0.1f, legs_height_max, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(legs_x1, legs_height_min, -body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(legs_x1 - 0.1f, legs_height_max, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(legs_x1 - 0.1f, legs_height_max, -body_thickness), leftNormal, Color.DarkOrange),

                        new VertexPositionNormalColor(new Vector3(legs_x2, legs_height_min, -body_thickness), rightNormal, Color.DarkOrange), // Right
                        new VertexPositionNormalColor(new Vector3(legs_x2 - 0.1f, legs_height_max, body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(legs_x2, legs_height_min, body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(legs_x2, legs_height_min, -body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(legs_x2 - 0.1f, legs_height_max, -body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(legs_x2 - 0.1f, legs_height_max, body_thickness), rightNormal, Color.DarkOrange),

                        // right leg
                        new VertexPositionNormalColor(new Vector3(-legs_x2, legs_height_min, -body_thickness), frontNormal, Color.Orange), // Front
                        new VertexPositionNormalColor(new Vector3(-legs_x2 + 0.1f, legs_height_max, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-legs_x1 + 0.1f, legs_height_max, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-legs_x2, legs_height_min, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-legs_x1 + 0.1f, legs_height_max, -body_thickness), frontNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-legs_x1, legs_height_min, -body_thickness), frontNormal, Color.Orange),

                        new VertexPositionNormalColor(new Vector3(-legs_x2, legs_height_min, body_thickness), backNormal, Color.Orange), // BACK
                        new VertexPositionNormalColor(new Vector3(-legs_x1 + 0.1f, legs_height_max, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-legs_x2 + 0.1f, legs_height_max, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-legs_x2, legs_height_min, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-legs_x1, legs_height_min, body_thickness), backNormal, Color.Orange),
                        new VertexPositionNormalColor(new Vector3(-legs_x1 + 0.1f, legs_height_max, body_thickness), backNormal, Color.Orange),

                        new VertexPositionNormalColor(new Vector3(-legs_x2 + 0.1f, legs_height_max, -body_thickness), topNormal, Color.OrangeRed), // Top
                        new VertexPositionNormalColor(new Vector3(-legs_x2 + 0.1f, legs_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-legs_x1 + 0.1f, legs_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-legs_x2 + 0.1f, legs_height_max, -body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-legs_x1 + 0.1f, legs_height_max, body_thickness), topNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-legs_x1 + 0.1f, legs_height_max, -body_thickness), topNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(-legs_x2, legs_height_min, -body_thickness), bottomNormal, Color.OrangeRed), // Bottom
                        new VertexPositionNormalColor(new Vector3(-legs_x1, legs_height_min, body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-legs_x2, legs_height_min, body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-legs_x2, legs_height_min, -body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-legs_x1, legs_height_min, -body_thickness), bottomNormal, Color.OrangeRed),
                        new VertexPositionNormalColor(new Vector3(-legs_x1, legs_height_min, body_thickness), bottomNormal, Color.OrangeRed),

                        new VertexPositionNormalColor(new Vector3(-legs_x2, legs_height_min, -body_thickness), leftNormal, Color.DarkOrange), // Left
                        new VertexPositionNormalColor(new Vector3(-legs_x2, legs_height_min, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-legs_x2 + 0.1f, legs_height_max, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-legs_x2, legs_height_min, -body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-legs_x2 + 0.1f, legs_height_max, body_thickness), leftNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-legs_x2 + 0.1f, legs_height_max, -body_thickness), leftNormal, Color.DarkOrange),

                        new VertexPositionNormalColor(new Vector3(-legs_x1, legs_height_min, -body_thickness), rightNormal, Color.DarkOrange), // Right
                        new VertexPositionNormalColor(new Vector3(-legs_x1 + 0.1f, legs_height_max, body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-legs_x1, legs_height_min, body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-legs_x1, legs_height_min, -body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-legs_x1 + 0.1f, legs_height_max, -body_thickness), rightNormal, Color.DarkOrange),
                        new VertexPositionNormalColor(new Vector3(-legs_x1 + 0.1f, legs_height_max, body_thickness), rightNormal, Color.DarkOrange),
                        
                    });

            effect = game.Content.Load<Effect>("Phong");
            inputLayout = VertexInputLayout.FromBuffer(0, vertices);
            this.game = game;
        }

        // Frame update.
        public override void Update(GameTime gameTime)
        {
            //pos.X += (float)game.accelerometerReading.AccelerationX;
            if (!game.isHintView && !game.isPaused)
            {
                this.rotation += (float)game.accelerometerReading.AccelerationX * 0.2f;   
                this.target = (Vector3)Vector3.Transform(initial_target, Matrix.RotationY(rotation)) + pos;

                if (game.mainPage.isRun())
                {
                    this.moveFront(checkWallCollision());
                }
            }

            World = Matrix.RotationY(rotation) * Matrix.Translation(pos);
            WorldInverseTranspose = Matrix.Transpose(Matrix.Invert(World));
        //    basicEffect.View = game.camera.View;
        //    basicEffect.World = Matrix.RotationY(rotation) * Matrix.Translation(pos);
        //    basicEffect.Projection = Matrix.PerspectiveFovLH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f, 100.0f);
        }

        public override void Draw(GameTime gameTime)
        {
            // Setup the effect parameters
            effect.Parameters["World"].SetValue(World);
            effect.Parameters["Projection"].SetValue(game.camera.Projection);
            effect.Parameters["View"].SetValue(game.camera.View);
            effect.Parameters["cameraPos"].SetValue(game.camera.pos);
            effect.Parameters["worldInvTrp"].SetValue(WorldInverseTranspose);
            effect.Parameters["lightPntPos"].SetValue(game.world.lantern.pos);
            effect.Parameters["intensity"].SetValue(energyIntensity());

            // Setup the vertices
            game.GraphicsDevice.SetVertexBuffer(vertices);
            game.GraphicsDevice.SetVertexInputLayout(inputLayout);

            effect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, vertices.ElementCount);
        }

        public override void Tapped(GestureRecognizer sender, TappedEventArgs args)
        {
        }

        public override void OnManipulationUpdated(GestureRecognizer sender, ManipulationUpdatedEventArgs args)
        {
            pos.X += (float)args.Delta.Translation.X / 100;
        }

        public void rotateLeft()
        {
            this.rotation -= 0.03f;
            this.target = (Vector3)Vector3.Transform(initial_target, Matrix.RotationY(rotation)) + pos;
        }

        public void rotateRight()
        {
            this.rotation += 0.03f;
            this.target = (Vector3)Vector3.Transform(initial_target, Matrix.RotationY(rotation)) + pos;
        }

        public void moveFront(bool checkCollide)
        {

            Vector3 a = Vector3.Normalize(target - pos) * velocity;
            bool allow_update = true;
            if (checkCollide)
            {
                // touching the boundary...... 
                // if next move is inside the circle, then... update else... stop moving
                // first check if user in bigCircle or small circle
                Vector3 temp_next_move = pos + a;
                if (isInBigCircle)
                {
                    Room room = this.game.world.getRoomByPlayerPosition(pos);
                    Vector3 room_pos = room.pos;
                    if (Vector3.Distance(room_pos, temp_next_move) < (6.0 - this.collisionRadius))
                    {
                        // allow update
                        allow_update = true;
                    }
                    else
                    {
                        allow_update = false;
                    }
                }
                else
                {
                    //in small circle
                    Door door = this.game.world.getDoorByPlayerPosition(pos);
                    if (Vector3.Distance(door.pos, temp_next_move) < (3.0 - this.collisionRadius))
                    {
                        allow_update = true;
                    }
                    else
                    {
                        allow_update = false;
                    }
                }
            }
            if (allow_update)
            {
                pos += a;
                target += a;
            }
            decreaseEnergy();
        }

        public void moveBack()
        {
            Vector3 a = Vector3.Normalize(target - pos) * velocity;
            pos -= a;
            target -= a;
        }

        public Vector3 getPos()
        {
            return this.pos;
        }

        public Vector3 getTarget()
        {
            return this.target;
        }

        public float getHeight()
        {
            return this.height;
        }

        private bool isInSafeZone(){
            if (this.isInBigCircle)
            {
                if (this.game.world.checkObjectInRoom(this.pos))
                {
                    // player is in some Room
                    Room room = this.game.world.getRoomByPlayerPosition(this.pos);
                    Vector3 roomPos = room.pos;
                    if (Vector3.Distance(roomPos, this.pos) > (6.0f - (this.collisionRadius)))
                    {
                        return false;
                    }
                    else
                    {
                        // check distance to the door... if close then open. 
                        List<Door> doorList = this.game.world.getDoorListByRoom(room);
                        foreach (Door door in doorList)
                        {
                            if (Vector3.Distance(door.pos, this.pos) < 3.0f)
                            {
                                // open the door
                                door.setOpeningFlag(true);
                                door.setOpenFlag(false);
                                door.setCloseFlag(false);
                            }
                        }
                        return true;
                    }
                }
            }
            else
            {
                // in small circle
                if (this.game.world.checkObjectInDoor(this.pos))
                {
                    Door door = this.game.world.getDoorByPlayerPosition(this.pos);
                    Vector3 doorPos = door.pos;
                    if (Vector3.Distance(doorPos, this.pos) > (3.0f - this.collisionRadius))
                    {
                        return false; 
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            return true;
        }

        public bool checkInBigCircleSafeAngleZone()
        {
            // when this function is called, 
            // we assume user in BigCircle and touching the edge already 
            // so we want to get the angle.. 
            Vector3 pos_2d = this.pos;
            Room room = this.game.world.getRoomByPlayerPosition(this.pos);
            Door door = this.game.world.getDoorByPlayerPosition(this.pos); 
            Vector3 room_pos_2d = room.pos;
            pos_2d.Y = 0;
            room_pos_2d.Y = 0;
            Vector3 dir_vector = Vector3.Subtract(pos_2d, room_pos_2d);
            Vector3 ref_vector = new Vector3(0,0,1);
            float dot_value = Vector3.Dot(dir_vector, ref_vector);
            dot_value = dot_value / (dir_vector.Length() * ref_vector.Length());
            double angle = radianToAngle(Math.Acos((double)dot_value));
            if (angle > 20 && angle < 40)
            {
                door.setOpeningFlag(true);
                door.setOpenFlag(false);
                door.setCloseFlag(false);
                return true;
            }
            else if (angle > 80 && angle < 100)
            {
                door.setOpeningFlag(true);
                door.setOpenFlag(false);
                door.setCloseFlag(false);
                return true;
            }
            else if (angle > 140 && angle < 160)
            {
                door.setOpeningFlag(true);
                door.setOpenFlag(false);
                door.setCloseFlag(false);
                return true;
            }
            else if (angle > 200 && angle < 220)
            {
                door.setOpeningFlag(true);
                door.setOpenFlag(false);
                door.setCloseFlag(false);
                return true;
            }
            else if (angle > 260 && angle < 280)
            {
                door.setOpeningFlag(true);
                door.setOpenFlag(false);
                door.setCloseFlag(false);
                return true;
            }
            else if (angle > 320 && angle < 340)
            {
                door.setOpeningFlag(true);
                door.setOpenFlag(false);
                door.setCloseFlag(false);
                return true;
            }
            return false;
        }

        public bool checkInSmallCircleSafeAngleZone()
        {
            Vector3 pos_2d = this.pos;
            Door door = this.game.world.getDoorByPlayerPosition(this.pos);
            Vector3 door_pos_2d = door.pos;
            pos_2d.Y = 0;
            door_pos_2d.Y = 0;
            Vector3 dir_vector = Vector3.Subtract(pos_2d, door_pos_2d);
            // get door type and set reference vector
            Vector3 ref_vector = new Vector3(0, 0, 0);
            DoorPosition type = door.getDoorPosition();
            if (type == DoorPosition.Left || type == DoorPosition.Right)
            {
                ref_vector = new Vector3(0, 0, 1);
            } 
            else if (type == DoorPosition.LeftTop || type==DoorPosition.RightBot)
            {
                ref_vector = new Vector3((float)Math.Tan(angleToRadian(30)), 0.0f, 1.0f);
            }
            else if (type == DoorPosition.RightTop || type == DoorPosition.LeftBot)
            {
                ref_vector = new Vector3((float)Math.Tan(angleToRadian(330)), 0.0f, 1.0f);
            }
  
            float dot_value = Vector3.Dot(dir_vector, ref_vector);
            dot_value = dot_value / (dir_vector.Length() * ref_vector.Length());
            double angle = radianToAngle(Math.Acos((double)dot_value));
            if ((angle >= 0 && angle < 10)||(angle>350 && angle<=360))
            {
                return false;
            }
            else if (angle > 173 && angle < 187)
            {
                return false;
            }
            // close the door
            door.setClosingFlag(true);
            door.setOpenFlag(false);
            door.setCloseFlag(false);
            return true;
        }

        public bool checkWallCollision()
        {
            // check if in safe zone first.. 
            if (!isInSafeZone())
            {
                if (this.isInBigCircle)
                {
                    if (checkInBigCircleSafeAngleZone())
                    {
                        // about to walk through the doors
                        // open the door
                        isInBigCircle = false;
                        // open door
                        return false;
                    }
                    else
                    {
                        // not in safe angle zone + collide to edge + in big circle
                        return true;
                    }
                }
                else
                {
                    if (checkInSmallCircleSafeAngleZone())
                    {
                        // about to leave the door area
                        // close the door
                        isInBigCircle = true;
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                    // in small circle
                }
            }
            else
            {
                // in safe zone, no collision happening... 
                return false;
            }
        }

        

        public void AddScore(int score)
        {
            this.score += score;
        }

        public int GetScore()
        {
            return this.score;
        }

        public void increaseEnergy()
        {
            energy += maxEnergy * 0.2f;
            if (energy > maxEnergy)
            {
                energy = maxEnergy;
            }
        }

        public void decreaseEnergy()
        {
            if (energy / maxEnergy * 100 > 15)
            {
                energy -= 0.1f;
            }
            else
            {
                energy -= 0.2f;
            }
            
            if (energy < 0)
            {
                energy = 0;
            }
        }

        public Vector3 getLanternPosition()
        {
            float distance = body_height_max - body_height_min - body_thickness;
            return (pos + new Vector3(-distance * (float)Math.Sin(rotation),0, -distance * (float)Math.Cos(rotation)));
        }

        public float energyIntensity()
        {
            return energy / maxEnergy * 1.7f;
        }

        private double angleToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        private double radianToAngle(double radian)
        {
            return ((radian * 180.0 / Math.PI) + 360) % 360;
        }
    }
}