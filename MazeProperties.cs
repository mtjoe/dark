﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dark
{
    public class MazeProperties
    {
        // Maze specs
        public int rows;
        public int roomsPerRow;

        // Room specs
        public float inner_radius;
        public float room_thickness;
        public float room_height;
        public float platform_level;

        // Door specs
        public float door_height;
        public float door_width;

    }
}
