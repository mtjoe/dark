﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;

namespace Dark
{
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;
    public class Cube : Shape
    {
        private Matrix World;
        private Matrix WorldInverseTranspose;
        private Vector3 pos;
        private Random random = new Random();

        public float collisionR = 5.0f;

        public Cube(DarkGame game)
        {
            

            Vector3 frontBottomLeft = new Vector3(-1.0f, -1.0f, -1.0f);
            Vector3 frontTopLeft = new Vector3(-1.0f, 1.0f, -1.0f);
            Vector3 frontTopRight = new Vector3(1.0f, 1.0f, -1.0f);
            Vector3 frontBottomRight = new Vector3(1.0f, -1.0f, -1.0f);
            Vector3 backBottomLeft = new Vector3(-1.0f, -1.0f, 1.0f);
            Vector3 backBottomRight = new Vector3(1.0f, -1.0f, 1.0f);
            Vector3 backTopLeft = new Vector3(-1.0f, 1.0f, 1.0f);
            Vector3 backTopRight = new Vector3(1.0f, 1.0f, 1.0f);
            
            Vector3 frontNormal = new Vector3(0.0f, 0.0f, -1.0f);
            Vector3 backNormal = new Vector3(0.0f, 0.0f, 1.0f);
            Vector3 topNormal = new Vector3(0.0f, 1.0f, 0.0f);
            Vector3 bottomNormal = new Vector3(0.0f, -1.0f, 0.0f);
            Vector3 leftNormal = new Vector3(-1.0f, 0.0f, 0.0f);
            Vector3 rightNormal = new Vector3(1.0f, 0.0f, 0.0f);

            vertices = Buffer.Vertex.New(
                game.GraphicsDevice,
                new[]
                    {
                    new VertexPositionNormalColor(frontBottomLeft, frontNormal, Color.Orange), // Front
                    new VertexPositionNormalColor(frontTopLeft, frontNormal, Color.Orange),
                    new VertexPositionNormalColor(frontTopRight, frontNormal, Color.Orange),
                    new VertexPositionNormalColor(frontBottomLeft, frontNormal, Color.Orange),
                    new VertexPositionNormalColor(frontTopRight, frontNormal, Color.Orange),
                    new VertexPositionNormalColor(frontBottomRight, frontNormal, Color.Orange),
                    new VertexPositionNormalColor(backBottomLeft, backNormal, Color.Orange), // BACK
                    new VertexPositionNormalColor(backTopRight, backNormal, Color.Orange),
                    new VertexPositionNormalColor(backTopLeft, backNormal, Color.Orange),
                    new VertexPositionNormalColor(backBottomLeft, backNormal, Color.Orange),
                    new VertexPositionNormalColor(backBottomRight, backNormal, Color.Orange),
                    new VertexPositionNormalColor(backTopRight, backNormal, Color.Orange),
                    new VertexPositionNormalColor(frontTopLeft, topNormal, Color.OrangeRed), // Top
                    new VertexPositionNormalColor(backTopLeft, topNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(backTopRight, topNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(frontTopLeft, topNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(backTopRight, topNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(frontTopRight, topNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(frontBottomLeft, bottomNormal, Color.OrangeRed), // Bottom
                    new VertexPositionNormalColor(backBottomRight, bottomNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(backBottomLeft, bottomNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(frontBottomLeft,bottomNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(frontBottomRight, bottomNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(backBottomRight, bottomNormal, Color.OrangeRed),
                    new VertexPositionNormalColor(frontBottomLeft, leftNormal, Color.DarkOrange), // Left
                    new VertexPositionNormalColor(backBottomLeft, leftNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(backTopLeft, leftNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(frontBottomLeft, leftNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(backTopLeft, leftNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(frontTopLeft, leftNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(frontBottomRight, rightNormal, Color.DarkOrange), // Right
                    new VertexPositionNormalColor(backTopRight, rightNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(backBottomRight, rightNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(frontBottomRight, rightNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(frontTopRight, rightNormal, Color.DarkOrange),
                    new VertexPositionNormalColor(backTopRight, rightNormal, Color.DarkOrange),
                });

            effect = game.Content.Load<Effect>("Phong-goal");

            inputLayout = VertexInputLayout.FromBuffer(0, vertices);
            this.game = game;
            this.setRandomPos();
        }

        public override void Update(GameTime gameTime)
        {
            // Rotate the cube.
            var time = (float)gameTime.TotalGameTime.TotalSeconds;
            World = (Matrix.RotationX(time) * Matrix.RotationY(time * 2.0f) * Matrix.RotationZ(time * .7f) * Matrix.Translation(pos));
            effect.Parameters["View"].SetValue(game.camera.View);
            WorldInverseTranspose = Matrix.Transpose(Matrix.Invert(World));
        }

        public override void Draw(GameTime gameTime)
        {
            // Setup the effect parameters
            effect.Parameters["World"].SetValue(World);
            effect.Parameters["Projection"].SetValue(game.camera.Projection);
            effect.Parameters["View"].SetValue(game.camera.View);
            effect.Parameters["cameraPos"].SetValue(game.camera.pos);
            effect.Parameters["worldInvTrp"].SetValue(WorldInverseTranspose);
            effect.Parameters["lightPntPos"].SetValue(pos);

            // Setup the vertices
            game.GraphicsDevice.SetVertexBuffer(vertices);
            game.GraphicsDevice.SetVertexInputLayout(inputLayout);

            // Apply the basic effect technique and draw the rotating cube
            effect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, vertices.ElementCount);
        }

        public override SharpDX.Vector3 getPos()
        {
            return this.pos;
        }

        public void setRandomPos()
        {
            this.pos = this.game.world.maze.getRandomRoom().getPos() + new Vector3(0, 3, 0);
        }
    }
}
