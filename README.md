# DARK - README #

### Game Overview ###

This application is a first-person, 3D, thriller, maze-runner game. The maze arena is made up of hexagonal rooms, all connected to one another by doors. Outside the arena is a deadly pool. There will be one or more enemy(ies) roaming around the arena, preying on the player.

Starting at the maximum (100%) energy, the player needs to find a gem, located in one of the rooms. When the player reaches this gem, 10 points will be added to the player, and the gem will move to a different location. Moving around the arena will cost the player energy. The player would be able to get a brief, yet crucial, look at the whole maze (location of the player themself and the gem) using the hint view. 

### Instructions ###

The goal of the player would be to find as many gems as possible without leaving the maze or touching the enemy, within the energy limit provided. Once the player leaves the maze or touches the enemy, the game will be over.

There are 2 ways for the player to navigate around the maze. The first (and intended way) is to use the accelerator in the device to rotate around and about in the maze, and pressing the run button to move through the maze. The second way is to use the player's keyboard keys to navigate around the maze, this method is for users whose device do not have an accelerometer in it.

The users will have several clues as to where the gem is located. The first is the distance indicator located in the top left of the maze, which will turn green as the player move closer to the gem, and red for the opposite. The player can choose to use the hint button to see the overall layout of the maze, and yes, that includes the player's and the gem's position in the maze. However, the hint view will cost the player 20% of their energy.

### Objects Modelling ###

The objects in the game generally have a set of vertices, an effect object (to set the different coordinates of the game, the basic effect and shading of the objects), etc. They all follow the phong/gouraud shading techniques.

The maze is broken up into rooms, and then 6 doors will then be added to each room. The rooms are basically made up of 6 walls and a floor, the doors are cuboids. These rooms and doors are then compiled into a maze object.

The player and enemy models are made to represent a human, with the use of cuboids and cubes. Textured layout is used in the enemy model, to represent it's face.

The gem model basically a cube, rotating about it's center.

### Graphics & Camera Motion ###

As user input (keyboard or device movement) for navigation is detected, the game will change the position or permutation of the player in the game's world coordinates (in the X-Z plane), which will be reflected on screen.

The camera, together with the light position, will follow the position and orientation of the player, hence causing it to move as the player moves. So, the camera basically acts as the eye of the player. When the player opts to use hint view, the camera position and target position will move such that the camera is looking down the maze from a  great height, such that the whole maze would be visible in the camera view space. This transition of the first-person view to the camera view and vice versa is animated. All the game objects View coordinates will follow that of the camera.