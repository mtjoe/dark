﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;
namespace Dark
{

    public class Camera
    {
        public Matrix View;
        public Matrix Projection;
        public DarkGame game;
        public Vector3 pos;
        public Vector3 target;
        public Vector3 oldPos;
        public Boolean firstPersonMode;

        public bool isHintView = false;
        public bool prevIsHintView = false;
        public Vector3 prevPos;
        public Vector3 prevTarget;

        // Hint View
        public Vector3 hintPos = new Vector3(5, 80, 0);

        public Vector3 hintTarget = new Vector3(0, 0, 0);

        public bool goingToHintView = false;
        public bool leavingHintView = false;
        public float transitionHintViewStartTime;
        public float transitionHintViewDuration = 400;

        // Ensures that all objects are being rendered from a consistent viewpoint
        public Camera(DarkGame game)
        {
            this.game = game;

            firstPersonMode = true;
            target = new Vector3(0, 4, 0);

            Projection = Matrix.PerspectiveFovLH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.01f, 1000.0f);
        }

        public void Initialize()
        {
            pos = this.game.world.player.getPos(); //+ new Vector3(0, this.game.world.player.getHeight(), 0);
            target = this.game.world.player.getTarget();// +new Vector3(0, this.game.world.player.getHeight(), 0);
            View =  Matrix.LookAtLH(pos, target, Vector3.UnitY);
        }

        // If the screen is resized, the projection matrix will change
        public void Update(GameTime gameTime)
        {
            float time = (float)gameTime.TotalGameTime.TotalMilliseconds;
            float timechange = (float)gameTime.ElapsedGameTime.TotalSeconds * 2;

            if (goingToHintView)
            {
                float timeDiff = (time - this.transitionHintViewStartTime) / transitionHintViewDuration;
                pos = prevPos + ((hintPos - prevPos) * timeDiff);
                target = prevTarget + ((hintTarget - prevTarget) * timeDiff);

                if (timeDiff >= 1)
                {
                    goingToHintView = false;

                }
            }
            else if (leavingHintView)
            {
                float timeDiff = (time - this.transitionHintViewStartTime) / transitionHintViewDuration;
                pos = prevPos + ((hintPos - prevPos) * (1 - timeDiff));
                target = prevTarget + ((hintTarget - prevTarget) * (1 - timeDiff));

                if (timeDiff >= 1)
                {
                    pos = prevPos;
                    target = prevTarget;
                    leavingHintView = false;

                }
            }
            else
            {
                if (isHintView && !prevIsHintView)
                {
                    // Initiate hintView
                    this.goingToHintView = true;
                    this.transitionHintViewStartTime = time;
                    prevPos = this.pos;
                    prevTarget = this.target;
                    prevIsHintView = isHintView;
                }
                else if (!isHintView && prevIsHintView)
                {
                    leavingHintView = true;
                    this.transitionHintViewStartTime = time;
                    prevIsHintView = isHintView;
                }
                else
                {
                    if (!isHintView)
                    {
                        pos = this.game.world.player.getPos() + (Vector3.Normalize(this.game.world.player.getTarget() - this.game.world.player.getPos()) * 0.5f) + new Vector3(0, 3, 0);
                        target = this.game.world.player.getTarget() + new Vector3(0, 3, 0);
                    }
                }
            }


            Projection = Matrix.PerspectiveFovLH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f, 100.0f);
            View = Matrix.LookAtLH(pos, target, Vector3.UnitY);
        }
    }
}
