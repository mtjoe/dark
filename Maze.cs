﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Input;

namespace Dark
{
    using SharpDX.Toolkit.Graphics;
    public class Maze : GameObject
    {

        BasicEffect basicEffect;
        private Matrix World;
        private Matrix WorldInverseTranspose;

        private float sin30 = 0.5f;
        private float cos30 = (float)Math.Sqrt(3) / 2;

        public MazeProperties config;

        // Maze specs
        private float inner_radius;
        private float thickness;
        private float radius_diff;
        private float outer_radius;
        
        // Spacing and counts
        private int rows;
        private int roomsPerRow;
        private float rowSpacing;
        private float colSpacing;

        public float MAXDISTANCE;

        private Random random = new Random();

        public List<GameObject> rooms, doors;

        public Dictionary<Room, List<Door>> roomDoorsDict;

        public Maze(DarkGame game, MazeProperties config)
        {
            roomDoorsDict = new Dictionary<Room, List<Door>>();
            this.config = config;
            this.game = game;

            inner_radius = config.inner_radius;
            thickness = config.room_thickness;
            radius_diff = thickness / cos30;
            outer_radius = inner_radius + radius_diff;

            rows = config.rows;
            roomsPerRow = config.roomsPerRow;
            colSpacing = 2 * outer_radius * cos30;
            rowSpacing = outer_radius + outer_radius * sin30;

            Vector3 frontBottomLeft = new Vector3(-1.0f, -1.0f, -1.0f);
            Vector3 frontTopLeft = new Vector3(-1.0f, 1.0f, -1.0f);
            Vector3 frontTopRight = new Vector3(1.0f, 1.0f, -1.0f);
            Vector3 frontBottomRight = new Vector3(1.0f, -1.0f, -1.0f);
            Vector3 backBottomLeft = new Vector3(-1.0f, -1.0f, 1.0f);
            Vector3 backBottomRight = new Vector3(1.0f, -1.0f, 1.0f);
            Vector3 backTopLeft = new Vector3(-1.0f, 1.0f, 1.0f);
            Vector3 backTopRight = new Vector3(1.0f, 1.0f, 1.0f);

            Vector3 frontNormal = new Vector3(0.0f, 0.0f, -1.0f);
            Vector3 backNormal = new Vector3(0.0f, 0.0f, 1.0f);
            Vector3 topNormal = new Vector3(0.0f, 1.0f, 0.0f);
            Vector3 bottomNormal = new Vector3(0.0f, -1.0f, 0.0f);
            Vector3 leftNormal = new Vector3(-1.0f, 0.0f, 0.0f);
            Vector3 rightNormal = new Vector3(1.0f, 0.0f, 0.0f);
            
            vertices = Buffer.Vertex.New(
                game.GraphicsDevice,
                new[]
                    {
                    new VertexPositionNormalColor(new Vector3(-100, -1, -100), topNormal, Color.Blue), // Top
                    new VertexPositionNormalColor(new Vector3(-100, -1, 100), topNormal, Color.Blue),
                    new VertexPositionNormalColor(new Vector3(100, -1, -100), topNormal, Color.Blue),
                    new VertexPositionNormalColor(new Vector3(100, -1, -100), topNormal, Color.Blue),
                    new VertexPositionNormalColor(new Vector3(-100, -1, 100), topNormal, Color.Blue),
                    new VertexPositionNormalColor(new Vector3(100, -1, 100), topNormal, Color.Blue),
                });

            basicEffect = new BasicEffect(game.GraphicsDevice)
            {
                View = game.camera.View,
                Projection = game.camera.Projection,
                World = Matrix.Identity,
                VertexColorEnabled = true,
                Alpha = 0.2f
            };
            //effect.Parameters[""].SetValue(new Color4())

            this.MAXDISTANCE = (float) Math.Sqrt(Math.Pow((rows * rowSpacing), 2) + Math.Pow(((roomsPerRow + 1) * colSpacing), 2));
            inputLayout = VertexInputLayout.FromBuffer(0, vertices);
            createMaze();
        }

        private void createMaze()
        {
            rooms = new List<GameObject>();
            doors = new List<GameObject>();

            Vector3 room_position;

            float x;
            float z = rows / 2 * rowSpacing;
            for (int row = 0; row < rows; row++)
            {
                if (row % 2 == 0)
                {
                    x = -roomsPerRow / 4 * colSpacing * 1.5f;
                    for (int col = 0; col < roomsPerRow; col++)
                    {
                        room_position = new Vector3(x, 0, z);
                        AddRoomAndDoors(room_position, row, col);
                        x += colSpacing;
                    }
                }
                else
                {
                    x = -roomsPerRow / 4 * colSpacing * 1.5f - outer_radius * cos30;
                    for (int col = 0; col < roomsPerRow+1; col++)
                    {
                        room_position = new Vector3(x, 0, z);
                        AddRoomAndDoors(room_position, row, col);
                        x += colSpacing;
                    }
                }
                
                
                z -= rowSpacing;
            }
        }

        public override void Update(GameTime gameTime)
        {
            float time = (float)gameTime.TotalGameTime.TotalSeconds;
            float timechange = (float)gameTime.ElapsedGameTime.TotalSeconds * 2;

            foreach (var mazeobject in rooms)
            {
                mazeobject.effect.Parameters["View"].SetValue(game.camera.View);
                mazeobject.effect.Parameters["lightPntPos"].SetValue(this.game.world.player.getPos() + new Vector3(0, 3, 0));
                mazeobject.Update(gameTime);
            }
            foreach (var mazeobject in doors)
            {
                mazeobject.effect.Parameters["View"].SetValue(game.camera.View);
                mazeobject.effect.Parameters["lightPntPos"].SetValue(this.game.world.player.getPos() + new Vector3(0,3,0));
                mazeobject.Update(gameTime);
            }

            basicEffect.World = Matrix.Identity;
            basicEffect.View = this.game.camera.View;
            WorldInverseTranspose = Matrix.Transpose(Matrix.Invert(World));

            //basicEffect.World = Matrix.RotationY(rotY) * Matrix.RotationX(rotX);
            //basicEffect.Projection = Matrix.PerspectiveFovLH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f, 100.0f);
            
        }

        public override void Draw(GameTime gameTime)
        {
            foreach (var room in rooms)
            {
                room.Draw(gameTime);
            }
            foreach (var door in doors)
            {
                door.Draw(gameTime);
            }

            
            game.GraphicsDevice.SetVertexBuffer(vertices);
            game.GraphicsDevice.SetVertexInputLayout(inputLayout);
            game.GraphicsDevice.SetBlendState(game.GraphicsDevice.BlendStates.AlphaBlend);

            basicEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, vertices.ElementCount);
        }

        public void AddRoomAndDoors(Vector3 room_position, int row, int col)
        {
            rooms.Add(new Room(game, room_position, config));
            doors.Add(new Door(game, room_position, DoorType.Normal, DoorPosition.Left, config));
            doors.Add(new Door(game, room_position, DoorType.Normal, DoorPosition.LeftTop, config));
            doors.Add(new Door(game, room_position, DoorType.Normal, DoorPosition.RightTop, config));
            if (row % 2 == 0)
            {
                if (col == roomsPerRow - 1)
                {
                    doors.Add(new Door(game, room_position, DoorType.Normal, DoorPosition.Right, config));
                }
            }
            else
            {
                if (col == roomsPerRow)
                {
                    doors.Add(new Door(game, room_position, DoorType.Normal, DoorPosition.Right, config));
                    doors.Add(new Door(game, room_position, DoorType.Normal, DoorPosition.RightBot, config));
                }
                if (col == 0)
                {
                    doors.Add(new Door(game, room_position, DoorType.Normal, DoorPosition.LeftBot, config));
                }
            }
            
            if (row == rows - 1)
            {
                doors.Add(new Door(game, room_position, DoorType.Normal, DoorPosition.LeftBot, config));
                doors.Add(new Door(game, room_position, DoorType.Normal, DoorPosition.RightBot, config));
            }
        }

        public Room getRandomRoom()
        {
            int randomRoomIndex = random.Next(0, this.rooms.Count);
            return (Room) rooms.ElementAt(randomRoomIndex);
        }

        public Room getEnemyRoom()
        {
            int randomRoomIndex = random.Next(0, this.rooms.Count);
            Room candidate = (Room)rooms.ElementAt(randomRoomIndex);
            while (Vector3.Distance(candidate.pos, game.world.player.pos) < config.inner_radius * 6f)
            {
                randomRoomIndex = random.Next(0, this.rooms.Count);
                candidate = (Room)rooms.ElementAt(randomRoomIndex);
            }
            return candidate;
        }

        public bool isInMaze(Vector3 position)
        {
            foreach (var room in rooms)
            {
                if (isInRoom(position, (Room) room))
                {
                    return true;
                }
            }
            return false;
        }

        public bool isInRoom(Vector3 position, Room room)
        {
            Vector3 dif = new Vector3(position.X - room.pos.X, 0, position.Z - room.pos.Z);
            if (dif.Length() < outer_radius)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
