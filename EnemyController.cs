﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;

namespace Dark
{
    using SharpDX.Toolkit.Graphics;


    // Enemy Controller class.
    class EnemyController : GameObject
    {

        // Constructor.
        public EnemyController(DarkGame game)
        {
            this.game = game;
        }


        // Frame update method.
        public override void Update(GameTime gameTime)
        {

        }

        // Method for when the game ends.
        private void gameOver()
        {
            game.Exit();
        }

        public override void Draw(GameTime gameTime)
        {

        }
    }
}