﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Toolkit;
using SharpDX;
namespace Dark
{
    // Enemy class
    // Basically just shoots randomly, see EnemyController for enemy movement.
    using SharpDX.Toolkit.Graphics;
    class Enemy : GameObject
    {

        private float orientation = 0, starting_orientation = -(float)Math.PI / 2;

        public string textureName;
        public Texture2D texture;

        Vector3 normal = new Vector3(0, 0, 0);
        // head
        private float head_height_max = 4.0f;
        private float head_height_min = 3.0f;
        private float head_size;
        // body
        private float body_height_max = 3.0f;
        private float body_height_min = 1.5f;
        private float body_width = 0.4f;
        private float body_thickness = 0.25f;
        // body part
        private float body_part_width = 0.25f;
        // arms
        private float arms_x1 = 0.5f;
        private float arms_x2 = 0.5f + 0.3f;
        // legs
        private float legs_height_max = 1.5f;
        private float legs_height_min = 0.0f;
        private float legs_x1 = 0.1f;
        private float legs_x2 = 0.1f + 0.4f;

        private float velocity;
        private float normalVelocity = 0.01f;
        private float chasingVelocity = 0.05f;
        private Vector3 target;
        private Vector3 previousTarget;

        private Random random = new Random();
        public Enemy(DarkGame game, Vector3 pos)
        {
            this.game = game;
            this.pos = pos;
            head_size = (head_height_max - head_height_min) / 2;

            velocity = normalVelocity;

            // for enemy AI... 
            target = getRandomRoom();

            collisionRadius = (body_part_width + body_thickness) / 4;
            // Specify the file name of the texture being used
            textureName = "clown.png";
            basicEffect = new BasicEffect(game.GraphicsDevice)
            {
                View = game.camera.View,
                Projection = game.camera.Projection,
                World = Matrix.Identity,
            };
            texture = game.Content.Load<Texture2D>(textureName);
            basicEffect.Texture = texture;
            basicEffect.TextureEnabled = true;
            basicEffect.VertexColorEnabled = false;

            Vector3 frontNormal = new Vector3(0.0f, 0.0f, -1.0f);
            Vector3 backNormal = new Vector3(0.0f, 0.0f, 1.0f);
            Vector3 topNormal = new Vector3(0.0f, 1.0f, 0.0f);
            Vector3 bottomNormal = new Vector3(0.0f, -1.0f, 0.0f);
            Vector3 leftNormal = new Vector3(-1.0f, 0.0f, 0.0f);
            Vector3 rightNormal = new Vector3(1.0f, 0.0f, 0.0f);

            texture_vertices = Buffer.Vertex.New(
                game.GraphicsDevice,
                new[]
                    {

                        // Head
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_min, -head_size - 0.6f), frontNormal , new Vector2(0.25f, 0.9f)), // Front
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_max, -head_size - 0.6f), frontNormal , new Vector2(0.25f, 0.25f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_max, -head_size - 0.6f), frontNormal , new Vector2(0.75f, 0.25f)),
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_min, -head_size - 0.6f), frontNormal , new Vector2(0.25f, 0.9f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_max, -head_size - 0.6f), frontNormal , new Vector2(0.75f, 0.25f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_min, -head_size - 0.6f), frontNormal , new Vector2(0.75f, 0.9f)),

                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_min, head_size - 0.6f), backNormal , new Vector2(0.0f, 0.0f)), // BACK
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_max, head_size - 0.6f), backNormal , new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_max, head_size - 0.6f), backNormal , new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_min, head_size - 0.6f), backNormal , new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_min, head_size - 0.6f), backNormal , new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_max, head_size - 0.6f), backNormal , new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_max, -head_size - 0.6f), topNormal, new Vector2(0.0f, 0.25f)), // Top
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_max, head_size - 0.6f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_max, head_size - 0.6f), topNormal, new Vector2(1.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_max, -head_size - 0.6f), topNormal, new Vector2(0.0f, 0.25f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_max, head_size - 0.6f), topNormal, new Vector2(1.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_max, -head_size - 0.6f), topNormal, new Vector2(1.0f, 0.25f)),

                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_min, -head_size - 0.6f), bottomNormal, new Vector2(0.0f, 0.0f)), // Bottom
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_min, head_size - 0.6f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_min, head_size - 0.6f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_min, -head_size - 0.6f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_min, -head_size - 0.6f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_min, head_size - 0.6f), bottomNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_min, -head_size - 0.6f), leftNormal, new Vector2(0.25f, 0.9f)), // Left
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_min, head_size - 0.6f), leftNormal, new Vector2(0.0f, 0.9f)),
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_max, head_size - 0.6f), leftNormal, new Vector2(0.0f, 0.25f)),
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_min, -head_size - 0.6f), leftNormal, new Vector2(0.25f, 0.9f)),
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_max, head_size - 0.6f), leftNormal, new Vector2(0.0f, 0.25f)),
                        new VertexPositionNormalTexture(new Vector3(-head_size, head_height_max, -head_size - 0.6f), leftNormal, new Vector2(0.25f, 0.25f)),

                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_min, -head_size - 0.6f), rightNormal, new Vector2(0.75f, 0.9f)), // Right
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_max, head_size - 0.6f), rightNormal, new Vector2(1.0f, 0.25f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_min, head_size - 0.6f), rightNormal, new Vector2(1.0f, 0.9f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_min, -head_size - 0.6f), rightNormal, new Vector2(0.75f, 0.9f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_max, -head_size - 0.6f), rightNormal, new Vector2(0.75f, 0.25f)),
                        new VertexPositionNormalTexture(new Vector3(head_size, head_height_max, head_size - 0.6f), rightNormal, new Vector2(1.0f, 0.25f)),

                        // body
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_min, -body_thickness), frontNormal, new Vector2(0.0f, 0.0f)), // Front
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_max, -body_thickness - 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_max, -body_thickness - 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_min, -body_thickness), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_max, -body_thickness - 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_min, -body_thickness), frontNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_min, body_thickness), backNormal, new Vector2(0.0f, 0.0f)), // BACK
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_max, body_thickness - 0.4f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_max, body_thickness - 0.4f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_min, body_thickness), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_min, body_thickness), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_max, body_thickness - 0.4f), backNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_max, -body_thickness - 0.4f), topNormal, new Vector2(0.0f, 0.0f)), // Top
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_max, body_thickness - 0.4f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_max, body_thickness - 0.4f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_max, -body_thickness - 0.4f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_max, body_thickness - 0.4f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_max, -body_thickness - 0.4f), topNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_min, -body_thickness), bottomNormal, new Vector2(0.0f, 0.0f)), // Bottom
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_min, body_thickness), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_min, body_thickness), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_min, -body_thickness), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_min, -body_thickness), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_min, body_thickness), bottomNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_min, -body_thickness), leftNormal, new Vector2(0.0f, 0.0f)), // Left
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_min, body_thickness), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_max, body_thickness - 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_min, -body_thickness), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_max, body_thickness - 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-body_width, body_height_max, -body_thickness - 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_min, -body_thickness), rightNormal, new Vector2(0.0f, 0.0f)), // Right
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_max, body_thickness - 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_min, body_thickness), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_min, -body_thickness), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_max, -body_thickness - 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(body_width, body_height_max, body_thickness - 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),

                        // left arm
                        new VertexPositionNormalTexture(new Vector3(arms_x1, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness) - 0.4f), frontNormal, new Vector2(0.0f, 0.0f)), // Front
                        new VertexPositionNormalTexture(new Vector3(arms_x1, head_height_max, -(body_height_max - body_height_min - body_thickness) - 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2, head_height_max, -(body_height_max - body_height_min - body_thickness) - 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x1, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(arms_x1 - 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), backNormal, new Vector2(0.0f, 0.0f)), // BACK
                        new VertexPositionNormalTexture(new Vector3(arms_x2 - 0.1f, body_height_max, body_thickness- 0.4f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x1 - 0.1f, body_height_max, body_thickness- 0.4f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x1 - 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2 - 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2 - 0.1f, body_height_max, body_thickness- 0.4f), backNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(arms_x1, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), topNormal, new Vector2(0.0f, 0.0f)), // Top
                        new VertexPositionNormalTexture(new Vector3(arms_x1 - 0.1f, body_height_max, body_thickness- 0.4f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2 - 0.1f, body_height_max, body_thickness- 0.4f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x1, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2 - 0.1f, body_height_max, body_thickness- 0.4f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), topNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(arms_x1, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), bottomNormal, new Vector2(0.0f, 0.0f)), // Bottom
                        new VertexPositionNormalTexture(new Vector3(arms_x2 - 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x1 - 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x1, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2 - 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), bottomNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(arms_x1, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), leftNormal, new Vector2(0.0f, 0.0f)), // Left
                        new VertexPositionNormalTexture(new Vector3(arms_x1 - 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x1 - 0.1f, body_height_max, body_thickness- 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x1, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x1 - 0.1f, body_height_max, body_thickness- 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x1, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(arms_x2, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), rightNormal, new Vector2(0.0f, 0.0f)), // Right
                        new VertexPositionNormalTexture(new Vector3(arms_x2 - 0.1f, body_height_max, body_thickness- 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2 - 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(arms_x2 - 0.1f, body_height_max, body_thickness- 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),


                        // right arm
                        new VertexPositionNormalTexture(new Vector3(-arms_x2, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), frontNormal, new Vector2(0.0f, 0.0f)), // Front
                        new VertexPositionNormalTexture(new Vector3(-arms_x2, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x2, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), frontNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-arms_x2 + 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), backNormal, new Vector2(0.0f, 0.0f)), // BACK
                        new VertexPositionNormalTexture(new Vector3(-arms_x1 + 0.1f, body_height_max, body_thickness- 0.4f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x2 + 0.1f, body_height_max, body_thickness- 0.4f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x2 + 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1 + 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1 + 0.1f, body_height_max, body_thickness- 0.4f), backNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-arms_x2, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), topNormal, new Vector2(0.0f, 0.0f)), // Top
                        new VertexPositionNormalTexture(new Vector3(-arms_x2 + 0.1f, body_height_max, body_thickness- 0.4f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1 + 0.1f, body_height_max, body_thickness- 0.4f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x2, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1 + 0.1f, body_height_max, body_thickness- 0.4f), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), topNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-arms_x2, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), bottomNormal, new Vector2(0.0f, 0.0f)), // Bottom
                        new VertexPositionNormalTexture(new Vector3(-arms_x1 + 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x2 + 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x2, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1 + 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), bottomNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-arms_x2, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), leftNormal, new Vector2(0.0f, 0.0f)), // Left
                        new VertexPositionNormalTexture(new Vector3(-arms_x2 + 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x2 + 0.1f, body_height_max, body_thickness- 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x2, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x2 + 0.1f, body_height_max, body_thickness- 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x2, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), leftNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-arms_x1, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), rightNormal, new Vector2(0.0f, 0.0f)), // Right
                        new VertexPositionNormalTexture(new Vector3(-arms_x1 + 0.1f, body_height_max, body_thickness- 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1 + 0.1f, body_height_max - 0.4f, body_thickness- 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1, head_height_max - 0.4f, -(body_height_max - body_height_min - body_thickness)- 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1, head_height_max, -(body_height_max - body_height_min - body_thickness)- 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-arms_x1 + 0.1f, body_height_max, body_thickness- 0.4f), rightNormal, new Vector2(0.0f, 0.0f)),

                        // left leg
                        new VertexPositionNormalTexture(new Vector3(legs_x1, legs_height_min, -body_thickness - 0.2f), frontNormal, new Vector2(0.0f, 0.0f)), // Front
                        new VertexPositionNormalTexture(new Vector3(legs_x1 - 0.1f, legs_height_max, -body_thickness), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2 - 0.1f, legs_height_max, -body_thickness), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x1, legs_height_min, -body_thickness - 0.2f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2 - 0.1f, legs_height_max, -body_thickness), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2, legs_height_min, -body_thickness - 0.2f), frontNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(legs_x1, legs_height_min, body_thickness - 0.2f), backNormal, new Vector2(0.0f, 0.0f)), // BACK
                        new VertexPositionNormalTexture(new Vector3(legs_x2 - 0.1f, legs_height_max, body_thickness), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x1 - 0.1f, legs_height_max, body_thickness), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x1, legs_height_min, body_thickness - 0.2f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2, legs_height_min, body_thickness - 0.2f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2 - 0.1f, legs_height_max, body_thickness), backNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(legs_x1 - 0.1f, legs_height_max, -body_thickness), topNormal, new Vector2(0.0f, 0.0f)), // Top
                        new VertexPositionNormalTexture(new Vector3(legs_x1 - 0.1f, legs_height_max, body_thickness), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2 - 0.1f, legs_height_max, body_thickness), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x1 - 0.1f, legs_height_max, -body_thickness), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2 - 0.1f, legs_height_max, body_thickness), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2 - 0.1f, legs_height_max, -body_thickness), topNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(legs_x1, legs_height_min, -body_thickness - 0.2f), bottomNormal, new Vector2(0.0f, 0.0f)), // Bottom
                        new VertexPositionNormalTexture(new Vector3(legs_x2, legs_height_min, body_thickness - 0.2f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x1, legs_height_min, body_thickness - 0.2f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x1, legs_height_min, -body_thickness - 0.2f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2, legs_height_min, -body_thickness - 0.2f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2, legs_height_min, body_thickness - 0.2f), bottomNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(legs_x1, legs_height_min, -body_thickness - 0.2f), leftNormal, new Vector2(0.0f, 0.0f)), // Left
                        new VertexPositionNormalTexture(new Vector3(legs_x1, legs_height_min, body_thickness - 0.2f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x1 - 0.1f, legs_height_max, body_thickness), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x1, legs_height_min, -body_thickness - 0.2f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x1 - 0.1f, legs_height_max, body_thickness), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x1 - 0.1f, legs_height_max, -body_thickness), leftNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(legs_x2, legs_height_min, -body_thickness - 0.2f), rightNormal, new Vector2(0.0f, 0.0f)), // Right
                        new VertexPositionNormalTexture(new Vector3(legs_x2 - 0.1f, legs_height_max, body_thickness), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2, legs_height_min, body_thickness - 0.2f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2, legs_height_min, -body_thickness - 0.2f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2 - 0.1f, legs_height_max, -body_thickness), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(legs_x2 - 0.1f, legs_height_max, body_thickness), rightNormal, new Vector2(0.0f, 0.0f)),

                        // right leg
                        new VertexPositionNormalTexture(new Vector3(-legs_x2, legs_height_min, -body_thickness + 0.2f), frontNormal, new Vector2(0.0f, 0.0f)), // Front
                        new VertexPositionNormalTexture(new Vector3(-legs_x2 + 0.1f, legs_height_max, -body_thickness), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1 + 0.1f, legs_height_max, -body_thickness), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x2, legs_height_min, -body_thickness + 0.2f), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1 + 0.1f, legs_height_max, -body_thickness), frontNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1, legs_height_min, -body_thickness + 0.2f), frontNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-legs_x2, legs_height_min, body_thickness + 0.2f), backNormal, new Vector2(0.0f, 0.0f)), // BACK
                        new VertexPositionNormalTexture(new Vector3(-legs_x1 + 0.1f, legs_height_max, body_thickness), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x2 + 0.1f, legs_height_max, body_thickness), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x2, legs_height_min, body_thickness + 0.2f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1, legs_height_min, body_thickness + 0.2f), backNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1 + 0.1f, legs_height_max, body_thickness), backNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-legs_x2 + 0.1f, legs_height_max, -body_thickness), topNormal, new Vector2(0.0f, 0.0f)), // Top
                        new VertexPositionNormalTexture(new Vector3(-legs_x2 + 0.1f, legs_height_max, body_thickness), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1 + 0.1f, legs_height_max, body_thickness), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x2 + 0.1f, legs_height_max, -body_thickness), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1 + 0.1f, legs_height_max, body_thickness), topNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1 + 0.1f, legs_height_max, -body_thickness), topNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-legs_x2, legs_height_min, -body_thickness + 0.2f), bottomNormal, new Vector2(0.0f, 0.0f)), // Bottom
                        new VertexPositionNormalTexture(new Vector3(-legs_x1, legs_height_min, body_thickness + 0.2f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x2, legs_height_min, body_thickness + 0.2f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x2, legs_height_min, -body_thickness + 0.2f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1, legs_height_min, -body_thickness + 0.2f), bottomNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1, legs_height_min, body_thickness + 0.2f), bottomNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-legs_x2, legs_height_min, -body_thickness + 0.2f), leftNormal, new Vector2(0.0f, 0.0f)), // Left
                        new VertexPositionNormalTexture(new Vector3(-legs_x2, legs_height_min, body_thickness + 0.2f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x2 + 0.1f, legs_height_max, body_thickness), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x2, legs_height_min, -body_thickness + 0.2f), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x2 + 0.1f, legs_height_max, body_thickness), leftNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x2 + 0.1f, legs_height_max, -body_thickness), leftNormal, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-legs_x1, legs_height_min, -body_thickness + 0.2f), rightNormal, new Vector2(0.0f, 0.0f)), // Right
                        new VertexPositionNormalTexture(new Vector3(-legs_x1 + 0.1f, legs_height_max, body_thickness), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1, legs_height_min, body_thickness + 0.2f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1, legs_height_min, -body_thickness + 0.2f), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1 + 0.1f, legs_height_max, -body_thickness), rightNormal, new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(-legs_x1 + 0.1f, legs_height_max, body_thickness), rightNormal, new Vector2(0.0f, 0.0f)),
            });


            inputLayout = VertexInputLayout.FromBuffer(0, texture_vertices);

        }



        public override void Update(GameTime gameTime)
        {
            if (!game.isHintView)
            {
                this.target = EnemyAI();
                Vector3 dir = (Vector3.Subtract(this.target, this.pos));
                dir.Normalize();
                Vector3 direction = new Vector3(dir.X, dir.Y, dir.Z);
                dir = Vector3.Multiply(dir, velocity);
                this.pos = Vector3.Add(this.pos, dir);

                float distance = Vector3.Distance(this.pos, game.world.player.pos);
                if (distance < collisionRadius)
                {
                    game.world.player.isDead = true;
                }

                if (direction.X == 0)
                {
                    if (direction.Z > 0)
                    {
                        orientation = -(float)Math.PI / 2;
                    }
                    else
                    {
                        orientation = (float)Math.PI / 2;
                    }
                }
                else if (direction.Z == 0)
                {
                    if (direction.X > 0)
                    {
                        orientation = 0;
                    }
                    else
                    {
                        orientation = (float)Math.PI;
                    }
                }
                else
                {
                    if (direction.X < 0)
                    {
                        if (direction.Z > 0)
                        {
                            orientation = -(float)(Math.Atan((double)(direction.Z / direction.X)) - Math.PI);
                        }
                        else
                        {
                            orientation = -(float)(Math.Atan((double)(direction.Z / direction.X)) + Math.PI);
                        }

                    }
                    else
                    {
                        orientation = -(float)Math.Atan((double)(direction.Z / direction.X));
                    }

                }
            }
            
       
            
            float time = (float)gameTime.TotalGameTime.TotalSeconds;
            float timechange = (float)gameTime.ElapsedGameTime.TotalSeconds * 2;
            basicEffect.View = game.camera.View;
            basicEffect.Projection = Matrix.PerspectiveFovLH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f, 100.0f);
            basicEffect.World = Matrix.RotationY(starting_orientation) * Matrix.RotationY(orientation) * Matrix.Translation(pos);
        }

        public override void Draw(GameTime gameTime)
        {
            // Setup the vertices
            game.GraphicsDevice.SetVertexBuffer(texture_vertices);
            game.GraphicsDevice.SetVertexInputLayout(inputLayout);

            // Apply the basic effect technique and draw the rotating cube
            basicEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, texture_vertices.ElementCount);
        }

        /**
         * Enemy AI 
         * If player is not around enemy, (by check distance between them),
         * if the distance is close, AI will return the current position of 
         * player to enemy and enemy will walk to that direction. 
         * 
         * If the player is not around enemy, we will just randomly return a
         * room position.No new target will be given to enemy untill enemy reach
         * the target or get close to the player.
         **/
        private Vector3 EnemyAI()
        {
            // get neighbour room.. work straigt, and start chasing player in certain distance

            // first we get the pos of the player and calculate distance
            // between them
            Vector3 currPlayerPosition = game.world.player.pos;
            float distance = (this.pos - currPlayerPosition).Length();
            float chasing_distance = 12.0f; // about two rooms apart
            if (distance <= chasing_distance)
            {
                velocity = chasingVelocity;
                return currPlayerPosition;
            }

            if ((this.pos - this.target).Length() < 0.01f)
            {
                // assign a new Room
                Room currRoom = this.game.world.getRoomByPlayerPosition(this.pos);
                List<Room> potentialRooms = this.game.world.roomToNeighbourRoomDict[currRoom];
                int randomRoomIndex = random.Next(0, potentialRooms.Count);
                Room targetRoom = (Room)potentialRooms.ElementAt(randomRoomIndex);
                // time to give enemy a new target
                this.target = targetRoom.pos;
            }
            // haven't meet the target, keep going
            //System.Diagnostics.Debug.WriteLine("TARGET" + target);
            //System.Diagnostics.Debug.WriteLine("POS" + pos);
            //System.Diagnostics.Debug.WriteLine("currPlayerPOs" + currPlayerPosition);
            velocity = normalVelocity;
            return target;
        }

        private Vector3 getRandomRoom()
        {

            return this.game.world.maze.getRandomRoom().getPos();
        }
    }
}
