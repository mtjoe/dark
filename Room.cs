﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Input;

namespace Dark
{
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;
    public class Room : GameObject
    {
        private Matrix World = Matrix.Identity;
        private Matrix WorldInverseTranspose;

        //Effect effect;
        private Vector3 eye, up;

        float out_radius, in_radius, thickness, radius_diff;
        float outer_margin, inner_margin, door_width;

        float out_x1, in_x1;
        float out_y1, out_y2, in_y1, in_y2;

        float door_height;
        float room_height;
        float platform_level;

        float sin30 = 0.5f;
        float cos30 = (float)Math.Sqrt(3)/2;

        float rotY = 0;
        float rotX = 0;

        public Room(DarkGame game, Vector3 pos, MazeProperties config)
        {
            this.pos = pos;

            thickness = config.room_thickness;
            radius_diff = thickness / cos30;

            door_height = config.door_height;
            room_height = config.room_height;
            door_width = config.door_width;

            in_radius = config.inner_radius;
            out_radius = in_radius + radius_diff;

            inner_margin = in_radius * sin30 - door_width / 2;
            outer_margin = inner_margin + thickness * sin30;

            platform_level = config.platform_level;

            in_x1 = in_radius * cos30;
            in_y1 = in_radius;
            in_y2 = in_radius * sin30;

            out_x1 = out_radius * cos30;
            out_y1 = out_radius;
            out_y2 = out_radius * sin30;
            
            collisionRadius = 0.125f;

            Vector3 normal = new Vector3(0, room_height / 2, 0);
            Vector3 normalLeftBottom = new Vector3(1, 0, (float) Math.Sqrt(3));
            Vector3 normalLeftTop = new Vector3(1, 0, (float) -Math.Sqrt(3));
            Vector3 normalLeft = new Vector3(1, 0, 0);
            Vector3 normalRightBottom = new Vector3(-1, 0, (float) Math.Sqrt(3));
            Vector3 normalRightTop = new Vector3(-1, 0, (float) -Math.Sqrt(3));
            Vector3 normalRight = new Vector3(-1, 0, 0);

            Vector3 normalTop = Vector3.UnitY;
            


            vertices = Buffer.Vertex.New(
                game.GraphicsDevice,
                new[]
                    {
                        // Outer side
                        // Right
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, -out_y2), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, -out_y2), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, out_y2), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, out_y2), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, out_y2), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, -out_y2), normalLeft, Color.Purple),

                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, -out_y2), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, -out_y2), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, -out_y2 + outer_margin), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, -out_y2 + outer_margin), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, -out_y2 + outer_margin), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, -out_y2), normalLeft, Color.Purple),

                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, out_y2 - outer_margin), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, out_y2 -outer_margin), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, out_y2), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, out_y2), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, out_y2), normalLeft, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, out_y2 - outer_margin), normalLeft, Color.Purple),


                        // Right Bottom
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, -out_y1), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, -out_y1), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, -out_y2), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, -out_y2), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, -out_y2), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, -out_y1), normalLeftTop, Color.Purple),

                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, -out_y1), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, -out_y1), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, door_height, -out_y1 + outer_margin * sin30), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, door_height, -out_y1 + outer_margin * sin30), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, 0.0f, -out_y1 + outer_margin * sin30), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, -out_y1), normalLeftTop, Color.Purple),

                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, 0.0f, -out_y2 - outer_margin * sin30), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, door_height, -out_y2 - outer_margin * sin30), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, -out_y2), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, -out_y2), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, -out_y2), normalLeftTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, 0.0f, -out_y2 - outer_margin * sin30), normalLeftTop, Color.Purple),


                        // Right Top
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, out_y2), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, out_y2), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, out_y1), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, out_y1), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, out_y1), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, out_y2), normalLeftBottom, Color.Purple),

                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, out_y2), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, out_y2), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, door_height, out_y2 + outer_margin * sin30), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, door_height, out_y2 + outer_margin * sin30), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, 0.0f, out_y2 + outer_margin * sin30), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, out_y2), normalLeftBottom, Color.Purple),

                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, 0.0f, out_y1 - outer_margin * sin30), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, door_height, out_y1 - outer_margin * sin30), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, out_y1), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, out_y1), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, out_y1), normalLeftBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, 0.0f, out_y1 - outer_margin * sin30), normalLeftBottom, Color.Purple),


                        // Left
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, out_y2), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, room_height, out_y2), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, room_height, -out_y2), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, room_height, -out_y2), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, -out_y2), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, out_y2), normalRight, Color.Purple),

                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, out_y2), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, out_y2), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, out_y2 - outer_margin), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, out_y2 - outer_margin), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, out_y2 - outer_margin), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, out_y2), normalRight, Color.Purple),

                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, -out_y2 + outer_margin), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, -out_y2 + outer_margin), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, -out_y2), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, -out_y2), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, -out_y2), normalRight, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, -out_y2 + outer_margin), normalRight, Color.Purple),


                        // Left Bottom
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, -out_y2), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, room_height, -out_y2), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, -out_y1), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, -out_y1), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, -out_y1), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, -out_y2), normalRightTop, Color.Purple),

                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, -out_y2), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, -out_y2), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, door_height, -out_y2 - outer_margin * sin30), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, door_height, -out_y2 - outer_margin * sin30), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, 0.0f, -out_y2 - outer_margin * sin30), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, -out_y2), normalRightTop, Color.Purple),

                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, 0.0f, -out_y1 + outer_margin * sin30), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, door_height, -out_y1 + outer_margin * sin30), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, -out_y1), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, -out_y1), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, -out_y1), normalRightTop, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, 0.0f, -out_y1 + outer_margin * sin30), normalRightTop, Color.Purple),


                        // Left Top
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, out_y1), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, out_y1), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, room_height, out_y2), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, room_height, out_y2), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, out_y2), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, out_y1), normalRightBottom, Color.Purple),

                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, out_y1), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, out_y1), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, door_height, out_y1 - outer_margin * sin30), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, door_height, out_y1 - outer_margin * sin30), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, 0.0f, out_y1 - outer_margin * sin30), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, out_y1), normalRightBottom, Color.Purple),

                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, 0.0f, out_y2 + outer_margin * sin30), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, door_height, out_y2 + outer_margin * sin30), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, out_y2), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, out_y2), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, out_y2), normalRightBottom, Color.Purple),
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, 0.0f, out_y2 + outer_margin * sin30), normalRightBottom, Color.Purple),


                        // Inner side
                        // Right
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, -in_y2), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, room_height, in_y2), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, room_height, -in_y2), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, room_height, in_y2), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, -in_y2), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, in_y2), normalRight, Color.DarkOliveGreen),

                        new VertexPositionNormalColor(new Vector3(in_x1, 0.0f, -in_y2), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, -in_y2+inner_margin), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, -in_y2), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, -in_y2+inner_margin), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, 0.0f, -in_y2), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, 0.0f, -in_y2+inner_margin), normalRight, Color.DarkOliveGreen),

                        new VertexPositionNormalColor(new Vector3(in_x1, 0.0f, in_y2-inner_margin), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, in_y2), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, in_y2-inner_margin), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, in_y2), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, 0.0f, in_y2-inner_margin), normalRight, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, 0.0f, in_y2), normalRight, Color.DarkOliveGreen),

                        // Right Bottom
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, -in_y1), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, room_height, -in_y2), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, -in_y1), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, room_height, -in_y2), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, -in_y1), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, -in_y2), normalRightBottom, Color.DarkOliveGreen),
                        
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, -in_y1), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, door_height, -in_y1 + inner_margin * sin30), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, -in_y1), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, door_height, -in_y1 + inner_margin * sin30), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, -in_y1), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, 0.0f, -in_y1 + inner_margin * sin30), normalRightBottom, Color.DarkOliveGreen),
                        
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, 0.0f, -in_y2 - inner_margin * sin30), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, -in_y2), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, door_height, -in_y2 - inner_margin * sin30), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, -in_y2), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, 0.0f, -in_y2 - inner_margin * sin30), normalRightBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, 0.0f, -in_y2), normalRightBottom, Color.DarkOliveGreen),

                        
                        // Right Top
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, in_y2), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, in_y1), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, room_height, in_y2), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, in_y1), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, in_y2), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, in_y1), normalRightTop, Color.DarkOliveGreen),
                        
                        new VertexPositionNormalColor(new Vector3(in_x1, 0.0f, in_y2), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, door_height, in_y2 + inner_margin * sin30), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, in_y2), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, door_height, in_y2 + inner_margin * sin30), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1, 0.0f, in_y2), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, 0.0f, in_y2 + inner_margin * sin30), normalRightTop, Color.DarkOliveGreen),
                        
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, 0.0f, in_y1 - inner_margin * sin30), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, in_y1), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, door_height, in_y1 - inner_margin * sin30), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, in_y1), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, 0.0f, in_y1 - inner_margin * sin30), normalRightTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, in_y1), normalRightTop, Color.DarkOliveGreen),
                        
                        // Left
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, in_y2), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, room_height, -in_y2), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, room_height, in_y2), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, room_height, -in_y2), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, in_y2), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, -in_y2), normalLeft, Color.DarkOliveGreen),
                        
                        new VertexPositionNormalColor(new Vector3(-in_x1, 0.0f, in_y2), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, in_y2 - inner_margin), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, in_y2), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, in_y2 - inner_margin), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, 0.0f, in_y2), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, 0.0f, in_y2 - inner_margin), normalLeft, Color.DarkOliveGreen),
                        
                        new VertexPositionNormalColor(new Vector3(-in_x1, 0.0f, -in_y2 + inner_margin), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, -in_y2), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, -in_y2 + inner_margin), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, -in_y2), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, 0.0f, -in_y2 + inner_margin), normalLeft, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, 0.0f, -in_y2), normalLeft, Color.DarkOliveGreen),
                        
                        // Left Bottom
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, -in_y2), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, -in_y1), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, room_height, -in_y2), normalLeftBottom, Color.DarkOliveGreen),                       
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, -in_y1), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, -in_y2), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, -in_y1), normalLeftBottom, Color.DarkOliveGreen),
                        
                        new VertexPositionNormalColor(new Vector3(-in_x1, 0.0f, -in_y2), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, door_height, -in_y2 - inner_margin * sin30), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, -in_y2), normalLeftBottom, Color.DarkOliveGreen),                        
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, door_height, -in_y2 - inner_margin * sin30), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, 0.0f, -in_y2), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, 0.0f, -in_y2 - inner_margin * sin30), normalLeftBottom, Color.DarkOliveGreen),
                        
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, 0.0f, -in_y1 + inner_margin * sin30), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, -in_y1), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, door_height, -in_y1 + inner_margin * sin30), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, -in_y1), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, 0.0f, -in_y1 + inner_margin * sin30), normalLeftBottom, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, -in_y1), normalLeftBottom, Color.DarkOliveGreen),
                        

                        // Left Top
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, in_y1), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, room_height, in_y2), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, in_y1), normalLeftTop, Color.DarkOliveGreen),                       
                        new VertexPositionNormalColor(new Vector3(-in_x1, room_height, in_y2), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, in_y1), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, in_y2), normalLeftTop, Color.DarkOliveGreen),                       

                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, in_y1), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, door_height, in_y1 - inner_margin * sin30), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, door_height, in_y1), normalLeftTop, Color.DarkOliveGreen),                       
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, door_height, in_y1 - inner_margin * sin30), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, in_y1), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, 0.0f, in_y1 - inner_margin * sin30), normalLeftTop, Color.DarkOliveGreen),
                        
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, 0.0f, in_y2 + inner_margin * sin30), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, in_y2), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, door_height, in_y2 + inner_margin * sin30), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, in_y2), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, 0.0f, in_y2 + inner_margin * sin30), normalLeftTop, Color.DarkOliveGreen),
                        new VertexPositionNormalColor(new Vector3(-in_x1, 0.0f, in_y2), normalLeftTop, Color.DarkOliveGreen),
                        
                        // Door frames
                        // Right
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, -out_y2 + outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, out_y2 - outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, in_y2 - inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, in_y2 - inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, -in_y2 + inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, -out_y2 + outer_margin), normal, Color.MediumPurple),
                        
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, -out_y2 + outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, -out_y2 + outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, -in_y2 + inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, -in_y2 + inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1, 0.0f, -in_y2 + inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, -out_y2 + outer_margin), normal, Color.MediumPurple),

                        new VertexPositionNormalColor(new Vector3(in_x1, 0.0f, in_y2 - inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1, door_height, in_y2 - inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, out_y2 - outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1, door_height, out_y2 - outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, out_y2 - outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1, 0.0f, in_y2 - inner_margin), normal, Color.MediumPurple),

                        // Right Bottom
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, door_height, -out_y1 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, door_height, -out_y2 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, door_height, -in_y2 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, door_height, -in_y2 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, door_height, -in_y1 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, door_height, -out_y1 + outer_margin * sin30), normal, Color.MediumPurple),

                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, 0.0f,-out_y1 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, door_height, -out_y1 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, door_height, -in_y1 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, door_height, -in_y1 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, 0.0f, -in_y1 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, 0.0f, -out_y1 + outer_margin * sin30), normal, Color.MediumPurple),

                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, 0.0f, -in_y2 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, door_height, -in_y2 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, door_height, -out_y2 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, door_height, -out_y2 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, 0.0f, -out_y2 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, 0.0f, -in_y2 - inner_margin * sin30), normal, Color.MediumPurple),

                        // Right Top
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, door_height, out_y2 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, door_height, out_y1 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, door_height, in_y1 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, door_height, in_y1 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, door_height, in_y2 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, door_height, out_y2 + outer_margin * sin30), normal, Color.MediumPurple),

                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, 0.0f, out_y2 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, door_height, out_y2 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, door_height, in_y2 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, door_height, in_y2 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(in_x1 - inner_margin * cos30, 0.0f, in_y2 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(out_x1 - outer_margin * cos30, 0.0f, out_y2 + outer_margin * sin30), normal, Color.MediumPurple),

                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, 0.0f, in_y1 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, door_height, in_y1 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, door_height, out_y1 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, door_height, out_y1 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + outer_margin * cos30, 0.0f, out_y1 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f + inner_margin * cos30, 0.0f, in_y1 - inner_margin * sin30), normal, Color.MediumPurple),

                        // Left
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, out_y2 - outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, -out_y2 + outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, -in_y2 + inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, -in_y2 + inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, in_y2 - inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, out_y2 - outer_margin), normal, Color.MediumPurple),
                        
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, out_y2 - outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, out_y2 - outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, in_y2 - inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, in_y2 - inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1, 0.0f, in_y2 - inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, out_y2 - outer_margin), normal, Color.MediumPurple),

                        new VertexPositionNormalColor(new Vector3(-in_x1, 0.0f, -in_y2 + inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1, door_height, -in_y2 + inner_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, -out_y2 + outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, door_height, -out_y2 + outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, -out_y2 + outer_margin), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1, 0.0f, -in_y2 + inner_margin), normal, Color.MediumPurple),

                         // Left Bottom
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, door_height, -out_y2 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, door_height, -out_y1 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, door_height, -in_y1 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, door_height, -in_y1 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, door_height, -in_y2 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, door_height, -out_y2 - outer_margin * sin30), normal, Color.MediumPurple),

                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, 0.0f,-out_y2 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, door_height, -out_y2 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, door_height, -in_y2 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, door_height, -in_y2 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, 0.0f, -in_y2 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, 0.0f, -out_y2 - outer_margin * sin30), normal, Color.MediumPurple),

                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, 0.0f, -in_y1 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, door_height, -in_y1 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, door_height, -out_y1 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, door_height, -out_y1 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, 0.0f, -out_y1 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, 0.0f, -in_y1 + inner_margin * sin30), normal, Color.MediumPurple),

                        // Left Top
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, door_height, out_y1 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, door_height, out_y2 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, door_height, in_y2 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, door_height, in_y2 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, door_height, in_y1 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, door_height, out_y1 - outer_margin * sin30), normal, Color.MediumPurple),

                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, 0.0f, out_y1 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, door_height, out_y1 - outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, door_height, in_y1 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, door_height, in_y1 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - inner_margin * cos30, 0.0f, in_y1 - inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(0.0f - outer_margin * cos30, 0.0f, out_y1 - outer_margin * sin30), normal, Color.MediumPurple),

                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, 0.0f, in_y2 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, door_height, in_y2 + inner_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, door_height, out_y2 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, door_height, out_y2 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-out_x1 + outer_margin * cos30, 0.0f, out_y2 + outer_margin * sin30), normal, Color.MediumPurple),
                        new VertexPositionNormalColor(new Vector3(-in_x1 + inner_margin * cos30, 0.0f, in_y2 + inner_margin * sin30), normal, Color.MediumPurple),

                        // Wall cellings
                        // Right
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, -out_y2), normalRight, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(in_x1, room_height, in_y2), normalRight, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, out_y2), normalRight, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(in_x1, room_height, in_y2), normalRight, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, -out_y2), normalRight, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(in_x1, room_height, -in_y2), normalRight, Color.Violet),

                        // Right Bottom
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, -out_y1), normalRightBottom, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, -in_y1), normalRightBottom, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, -out_y2), normalRightBottom, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, -in_y1), normalRightBottom, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(in_x1, room_height, -in_y2), normalRightBottom, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, -out_y2), normalRightBottom, Color.Violet),
                        
                        // Right Top
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, out_y2), normalRightTop, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(in_x1, room_height, in_y2), normalRightTop, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, in_y1), normalRightTop, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, in_y1), normalRightTop, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, out_y1), normalRightTop, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(out_x1, room_height, out_y2), normalRightTop, Color.Violet),

                        // Left
                        new VertexPositionNormalColor(new Vector3(-out_x1, room_height, -out_y2), normalLeft, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(-out_x1, room_height, out_y2), normalLeft, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(-in_x1, room_height, in_y2), normalLeft, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(-in_x1, room_height, in_y2), normalLeft, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(-in_x1, room_height, -in_y2), normalLeft, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(-out_x1, room_height, -out_y2), normalLeft, Color.Violet),
                        
                        // Left Bottom
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, -out_y1), normalLeftBottom, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(-out_x1, room_height, -out_y2), normalLeftBottom, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(-in_x1, room_height, -in_y2), normalLeftBottom, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(-in_x1, room_height, -in_y2), normalLeftBottom, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, -in_y1), normalLeftBottom, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, -out_y1), normalLeftBottom, Color.Violet),
                        
                        // Left Top
                        new VertexPositionNormalColor(new Vector3(-out_x1, room_height, out_y2), normal, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, out_y1), normal, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, in_y1), normal, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(0.0f, room_height, in_y1), normal, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(-in_x1, room_height, in_y2), normal, Color.Violet),
                        new VertexPositionNormalColor(new Vector3(-out_x1, room_height, out_y2), normal, Color.Violet),

                        // Floor
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, out_y2), normalTop, Color.DarkSlateGray),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, out_y1), normalTop, Color.DarkSlateGray),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, out_y2), normalTop, Color.DarkSlateGray),

                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, -out_y2), normalTop, Color.DarkSlateGray),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, -out_y2), normalTop, Color.DarkSlateGray),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, out_y2), normalTop, Color.DarkSlateGray),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, out_y2), normalTop, Color.DarkSlateGray),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, out_y2), normalTop, Color.DarkSlateGray),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, -out_y2), normalTop, Color.DarkSlateGray),

                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, -out_y2), normalTop, Color.DarkSlateGray),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, -out_y1), normalTop, Color.DarkSlateGray),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, -out_y2), normalTop, Color.DarkSlateGray),

                        // Platforms
                        // Right
                        new VertexPositionNormalColor(new Vector3(out_x1, platform_level, -out_y2), normalLeft, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, -out_y2), normalLeft, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, out_y2), normalLeft, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, out_y2), normalLeft, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, platform_level, out_y2), normalLeft, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, platform_level, -out_y2), normalLeft, Color.Brown),

                        // Right Bottom
                        new VertexPositionNormalColor(new Vector3(0.0f, platform_level, -out_y1), normalLeftTop, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, -out_y1), normalLeftTop, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, -out_y2), normalLeftTop, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, -out_y2), normalLeftTop, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, platform_level, -out_y2), normalLeftTop, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(0.0f, platform_level, -out_y1), normalLeftTop, Color.Brown),

                        // Right Top
                        new VertexPositionNormalColor(new Vector3(out_x1, platform_level, out_y2), normalLeftBottom, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, 0.0f, out_y2), normalLeftBottom, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, out_y1), normalLeftBottom, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, out_y1), normalLeftBottom, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(0.0f, platform_level, out_y1), normalLeftBottom, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, platform_level, out_y2), normalLeftBottom, Color.Brown),

                        // Left
                        new VertexPositionNormalColor(new Vector3(-out_x1, platform_level, out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, -out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, -out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, platform_level, -out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, platform_level, out_y2), normal, Color.Brown),

                        // Left Bottom
                        new VertexPositionNormalColor(new Vector3(-out_x1, platform_level, -out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, -out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, -out_y1), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, -out_y1), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(0.0f, platform_level, -out_y1), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, platform_level, -out_y2), normal, Color.Brown),

                        // Left Top
                        new VertexPositionNormalColor(new Vector3(0.0f, platform_level, out_y1), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(0.0f, 0.0f, out_y1), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, 0.0f, out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, platform_level, out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(0.0f, platform_level, out_y1), normal, Color.Brown),

                        // Bottom
                        new VertexPositionNormalColor(new Vector3(-out_x1, platform_level, out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, platform_level, out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(0.0f, platform_level, out_y1), normal, Color.Brown),
                        
                        new VertexPositionNormalColor(new Vector3(out_x1, platform_level, -out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, platform_level, out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, platform_level, -out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, platform_level, out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, platform_level, -out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(out_x1, platform_level, out_y2), normal, Color.Brown),
                        
                        new VertexPositionNormalColor(new Vector3(out_x1, platform_level, -out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(-out_x1, platform_level, -out_y2), normal, Color.Brown),
                        new VertexPositionNormalColor(new Vector3(0.0f, platform_level, -out_y1), normal, Color.Brown),
                        
                    });

            eye = new Vector3(0, 0, -10);
            up = new Vector3(Vector3.UnitY.X, Vector3.UnitY.Y, Vector3.UnitY.Z);

            effect = game.Content.Load<Effect>("Gouraud");

            //effect.Parameters["View"].SetValue(game.camera.View);
            //effect.Parameters["Projection"].SetValue(game.camera.Projection);
            //effect.Parameters["World"].SetValue(Matrix.Identity);
            //effect.Parameters["VertexColorEnabled"].SetValue(true);

            inputLayout = VertexInputLayout.FromBuffer(0, vertices);
            this.game = game;
        }

        public override void Update(GameTime gameTime)
        {
            float time = (float)gameTime.TotalGameTime.TotalSeconds;
            float timechange = (float)gameTime.ElapsedGameTime.TotalSeconds * 2;

            World = Matrix.Translation(pos) * Matrix.RotationY(rotY) * Matrix.RotationX(rotX);
            WorldInverseTranspose = Matrix.Transpose(Matrix.Invert(World));
         }

        public override void Draw(GameTime gameTime)
        {
            // Setup the effect parameters
            effect.Parameters["World"].SetValue(World);
            effect.Parameters["Projection"].SetValue(game.camera.Projection);
            effect.Parameters["View"].SetValue(game.camera.View);
            effect.Parameters["cameraPos"].SetValue(game.camera.pos);
            effect.Parameters["worldInvTrp"].SetValue(WorldInverseTranspose);
            effect.Parameters["lightPntPos"].SetValue(game.world.lantern.pos);
            effect.Parameters["intensity"].SetValue(game.world.player.energyIntensity());

            // Setup the vertices
            game.GraphicsDevice.SetVertexBuffer(vertices);
            game.GraphicsDevice.SetVertexInputLayout(inputLayout);

            // Apply the basic effect technique and draw the rotating cube
            effect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, vertices.ElementCount);
        }

        public float getOutRadius()
        {
            return this.out_radius;
        }
        public float getInRadius()
        {
            return this.in_radius;
        }

        public Vector3 getPos()
        {
            return this.pos;
        }
    }
}
